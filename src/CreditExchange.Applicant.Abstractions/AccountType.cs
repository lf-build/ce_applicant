﻿namespace CreditExchange.Applicant
{
    public enum AccountType
    {
        Savings = 1,
        Current = 2,
        Checking = 3
    }
}
