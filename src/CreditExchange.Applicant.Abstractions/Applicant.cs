﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;

namespace CreditExchange.Applicant
{
    public class Applicant : Aggregate, IApplicant
    {
        public Applicant () { }

        public Applicant (IApplicantRequest applicant)
        {
            if (applicant == null)
                throw new ArgumentNullException (nameof (applicant));

            UserId = applicant.UserId;
            Salutation = applicant.Salutation;
            FirstName = applicant.FirstName;
            LastName = applicant.LastName;
            MiddleName = applicant.MiddleName;
            DateOfBirth = applicant.DateOfBirth;
            EmailAddresses = applicant.EmailAddresses;
            PhoneNumbers = applicant.PhoneNumbers;
            Addresses = applicant.Addresses;
            AadhaarNumber = applicant.AadhaarNumber;
            PermanentAccountNumber = applicant.PermanentAccountNumber?.ToUpper ();
            Gender = applicant.Gender;
            MaritalStatus = applicant.MaritalStatus;
            EmploymentDetails = applicant.EmploymentDetails;
            BankInformation = applicant.BankInformation;
            HighestEducationInformation = applicant.HighestEducationInformation;
            IncomeInformation = applicant.IncomeInformation;
            CanReapplyBy = applicant.CanReapplyBy;
        }

        public string UserId { get; set; }
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public DateTimeOffset DateOfBirth { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IEmailAddress, EmailAddress>))]
        public List<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public List<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        public string AadhaarNumber { get; set; }
        public string PermanentAccountNumber { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IIncomeInformation, IncomeInformation>))]
        public List<IIncomeInformation> IncomeInformation { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IEmploymentDetail, EmploymentDetail>))]
        public List<IEmploymentDetail> EmploymentDetails { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IBankInformation, BankInformation>))]
        public List<IBankInformation> BankInformation { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IEducationInformation, EducationInformation>))]
        public IEducationInformation HighestEducationInformation { get; set; }

        public DateTime? CanReapplyBy { get; set; }
    }
}