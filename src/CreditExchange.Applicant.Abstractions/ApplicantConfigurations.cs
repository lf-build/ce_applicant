﻿namespace CreditExchange.Applicant
{
    public class ApplicantConfigurations : IApplicantConfigurations
    {
        public string[] DefaultRoles { get; set; }
        public int ReApplyExtensionDays { get; set; } 
    }
}
