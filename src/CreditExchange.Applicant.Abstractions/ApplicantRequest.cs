﻿
namespace CreditExchange.Applicant
{
    public class ApplicantRequest : Applicant,IApplicantRequest
    {
        public ApplicantRequest(){ }

        public ApplicantRequest(IApplicantRequest applicantRequest) : base(applicantRequest)
        {
            UserName = applicantRequest.UserName;
            Password = applicantRequest.Password;
        }

        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

