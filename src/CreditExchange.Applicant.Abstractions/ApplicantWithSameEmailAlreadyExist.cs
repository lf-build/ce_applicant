﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Applicant
{
    [Serializable]
    public class ApplicantWithSameEmailAlreadyExist : Exception
    {
        public ApplicantWithSameEmailAlreadyExist()
        {
        }

        public ApplicantWithSameEmailAlreadyExist(string message) : base(message)
        {
        }

        public ApplicantWithSameEmailAlreadyExist(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ApplicantWithSameEmailAlreadyExist(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
