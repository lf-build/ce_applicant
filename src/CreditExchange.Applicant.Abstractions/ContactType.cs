﻿
namespace CreditExchange.Applicant
{
    public enum ContactType
    {
        Home = 1,
        Work = 2,
        Alternate = 3
    }
}
