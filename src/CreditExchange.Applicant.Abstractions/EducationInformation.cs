﻿namespace CreditExchange.Applicant
{
    public class EducationInformation : IEducationInformation
    {
        public string LevelOfEducation { get; set; }
        public string EducationalInstitution { get; set; }
    }
}