﻿namespace CreditExchange.Applicant
{
    public class EmailAddress : IEmailAddress
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public EmailType EmailType { get; set; }
        public bool IsDefault { get; set; }
    }
}
