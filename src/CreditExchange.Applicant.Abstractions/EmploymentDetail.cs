﻿using System.Collections.Generic;
using Newtonsoft.Json;

using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Applicant
{
    public class EmploymentDetail : IEmploymentDetail
    {
        public string EmploymentId { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        public List<IAddress> Addresses { get; set; }

        public string Designation { get; set; }

        public int LengthOfEmploymentInMonths { get; set; }

        public string Name { get; set; }

        public string WorkEmail { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        public List<IPhoneNumber> WorkPhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        public IIncomeInformation IncomeInformation { get; set; }

        public TimeBucket AsOfDate { get; set; }

        public EmploymentStatus EmploymentStatus { get; set; }

        public string CinNumber { get; set; }

    }
}
