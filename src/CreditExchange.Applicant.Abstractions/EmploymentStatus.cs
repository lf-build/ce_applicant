﻿namespace CreditExchange.Applicant
{
    public enum EmploymentStatus
    {
        Salaried=1,
        SelfEmployed=2,
        Retired=3,
        Other=4
    }
}
