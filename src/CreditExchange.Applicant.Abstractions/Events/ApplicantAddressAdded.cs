﻿

namespace CreditExchange.Applicant
{
    public class ApplicantAddressAdded
    {
        public IAddress AddresseAdded { get; set; }
        public string ApplicantId { get; set; }
    }
}
