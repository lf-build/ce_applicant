﻿namespace CreditExchange.Applicant
{
    public class ApplicantAddressDeleted
    {
        public IAddress AddresseDeleted { get; set; }
        public string ApplicantId { get; set; }
    }
}
