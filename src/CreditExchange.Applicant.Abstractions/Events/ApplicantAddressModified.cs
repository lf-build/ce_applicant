﻿namespace CreditExchange.Applicant
{
    public class ApplicantAddressModified
    {
        public IAddress NewAddresses { get; set; }

        public IAddress OldAddresses { get; set; }
        public string ApplicantId { get; set; }
    }
}
