﻿namespace CreditExchange.Applicant
{
    public class ApplicantBankAdded
    {
        public IBankInformation BankInformationAdded { get; set; }
        public string ApplicantId { get; set; }
    }
}
