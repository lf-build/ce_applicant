﻿namespace CreditExchange.Applicant
{
    public class ApplicantBankDeleted
    {
        public IBankInformation BankInformationDeleted { get; set; }
        public string ApplicantId { get; set; }
    }
}
