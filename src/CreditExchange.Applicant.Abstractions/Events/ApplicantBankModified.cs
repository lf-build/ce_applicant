﻿

namespace CreditExchange.Applicant
{
    public class ApplicantBankModified
    {
        public IBankInformation NewBankInformation { get; set; }
        public IBankInformation OldBankInformation { get; set; }
        public string ApplicantId { get; set; }
    }
}
