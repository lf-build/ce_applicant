﻿namespace CreditExchange.Applicant
{
    public class ApplicantCreated
    {
        public IApplicant Applicant { get; set; }
    }
}
