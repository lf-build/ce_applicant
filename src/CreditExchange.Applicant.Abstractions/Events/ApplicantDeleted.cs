﻿namespace CreditExchange.Applicant
{
    public class ApplicantDeleted
    {
        public IApplicant Applicant { get; set; }
    }
}
