﻿namespace CreditExchange.Applicant
{
    public class ApplicantEmailAdded
    {
        public IEmailAddress EmailAddresseAdded { get; set; }
        public string ApplicantId { get; set; }
    }
}
