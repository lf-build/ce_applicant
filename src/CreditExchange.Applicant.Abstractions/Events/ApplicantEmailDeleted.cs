﻿namespace CreditExchange.Applicant
{
    public class ApplicantEmailDeleted
    {
        public IEmailAddress EmailAddresseDeleted { get; set; }
        public string ApplicantId { get; set; }
    }
}
