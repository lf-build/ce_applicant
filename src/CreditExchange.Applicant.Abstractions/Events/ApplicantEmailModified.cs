﻿

namespace CreditExchange.Applicant
{
    public class ApplicantEmailModified
    {
        public IEmailAddress NewEmailAddress { get; set; }

        public IEmailAddress OldEmailAddress { get; set; }
        public string ApplicantId { get; set; }
    }
}
