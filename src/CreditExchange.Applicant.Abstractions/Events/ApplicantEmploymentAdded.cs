﻿namespace CreditExchange.Applicant
{
    public class ApplicantEmploymentAdded
    {
        public IEmploymentDetail EmploymentDetailAdded { get; set; }
        public string ApplicantId { get; set; }
    }
}
