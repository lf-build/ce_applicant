﻿namespace CreditExchange.Applicant
{
    public class ApplicantEmploymentDeleted
    {
        public IEmploymentDetail EmploymentDetailDeleted { get; set; }
        public string ApplicantId { get; set; }
    }
}
