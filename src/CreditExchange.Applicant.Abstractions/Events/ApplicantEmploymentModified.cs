﻿

namespace CreditExchange.Applicant
{
    public class ApplicantEmploymentModified
    {
        public IEmploymentDetail NewEmploymentDetail { get; set; }
        public IEmploymentDetail OldEmploymentDetail { get; set; }
        public string ApplicantId { get; set; }
    }
}
