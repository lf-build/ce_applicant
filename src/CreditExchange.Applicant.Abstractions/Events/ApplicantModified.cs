﻿namespace CreditExchange.Applicant
{
    public class ApplicantModified
    {
        public IApplicant Applicant { get; set; }
    }
}