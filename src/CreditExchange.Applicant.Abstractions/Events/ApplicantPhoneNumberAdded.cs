﻿namespace CreditExchange.Applicant
{
    public class ApplicantPhoneNumberAdded
    {
        public IPhoneNumber PhoneNumberAdded { get; set; }
        public string ApplicantId { get; set; }
    }
}
