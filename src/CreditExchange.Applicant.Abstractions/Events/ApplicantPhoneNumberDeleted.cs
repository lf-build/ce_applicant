﻿namespace CreditExchange.Applicant
{
    public class ApplicantPhoneNumberDeleted
    {
        public IPhoneNumber PhoneNumberDeleted { get; set; }
        public string ApplicantId { get; set; }
    }
}
