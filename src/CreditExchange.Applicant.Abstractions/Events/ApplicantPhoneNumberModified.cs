﻿
namespace CreditExchange.Applicant
{
    public class ApplicantPhoneNumberModified
    {
        public IPhoneNumber NewPhoneNumber { get; set; }
        public IPhoneNumber OldPhoneNumber { get; set; }
        public string ApplicantId { get; set; }
    }
}
