namespace CreditExchange.Applicant
{
    public interface IAddress
    {
        string AddressId { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string AddressLine3 { get; set; }
        string AddressLine4 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string PinCode { get; set; }
        string Country { get; set; }
        AddressType AddressType { get; set; }
        bool IsDefault { get; set; }
        string LandMark { get; set; }
        string Location { get; set; }
    }
}