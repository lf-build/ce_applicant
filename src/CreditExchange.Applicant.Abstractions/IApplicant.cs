﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Applicant
{
    public interface IApplicant : IAggregate
    {
        string UserId { get; set; }
        string Salutation { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string LastName { get; set; }
        DateTimeOffset DateOfBirth { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmailAddress, EmailAddress>))]
        List<IEmailAddress> EmailAddresses { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        List<IPhoneNumber> PhoneNumbers { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Addresses { get; set; }

        string AadhaarNumber { get; set; }

        string PermanentAccountNumber { get; set; }

        Gender Gender { get; set; }

        MaritalStatus MaritalStatus { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IIncomeInformation, IncomeInformation>))]
        List<IIncomeInformation> IncomeInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankInformation, BankInformation>))]
        List<IBankInformation> BankInformation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IEmploymentDetail, EmploymentDetail>))]
        List<IEmploymentDetail> EmploymentDetails { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEducationInformation, EducationInformation>))]
        IEducationInformation HighestEducationInformation { get; set; }
        DateTime? CanReapplyBy { get; set; }
    }
}
