﻿namespace CreditExchange.Applicant
{
    public interface IApplicantConfigurations
    {
        string[] DefaultRoles { get; set; }
        int ReApplyExtensionDays { get; set; }
    }
}