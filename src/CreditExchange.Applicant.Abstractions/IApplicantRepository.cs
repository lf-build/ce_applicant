﻿using System.Collections.Generic;
using System.Threading.Tasks;

using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Applicant
{
    public interface IApplicantRepository : IRepository<IApplicant>
    {
        Task<IApplicant> GetByApplicantId(string applicantId);

        Task AssociateUser(string applicantId, string userId);

        Task UpdateAddress(string applicantId, List<IAddress> addresses);

        Task UpdateEmailAddress(string applicantId, List<IEmailAddress> emailAddresses);

        Task UpdatePhoneNumber(string applicantId, List<IPhoneNumber> phoneNumbers);

        Task UpdateEmployment(string applicantId, List<IEmploymentDetail> employmentDetails);

        Task UpdateBanks(string applicantId, List<IBankInformation> banks);

        Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest);

        Task<IApplicant> UpdateApplicant(string applicantId, IApplicant updateApplicantRequest);

        Task UpdateEducation(string applicantId, IEducationInformation educationDetails);

        Task SetReApplyFlag(string applicantId, DateTime? reApplyBy);

        Task UpdateAadhaarNumber(string applicantId, string adhaarnumber);
    }
}
