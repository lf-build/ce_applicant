﻿
namespace CreditExchange.Applicant
{
    public interface IApplicantRequest : IApplicant
    {
        string UserName { get; set; }
        string Password { get; set; }
    }
}