﻿using System.Threading.Tasks;

namespace CreditExchange.Applicant
{
    public interface IApplicantService
    {
        Task<IApplicant> Add(IApplicantRequest applicant);
        Task<IApplicant> UpdateApplicant(string applicantNumber, IUpdateApplicantRequest applicantRequest);
        Task<IApplicant> Get(string applicantId);
        Task Delete(string applicantId);

        Task AssociateUser(string applicantId, string userId);

        Task<IApplicant> AddAddress(string applicantId, IAddress address);
        Task UpdateAddress(string applicantId, string addressId, IAddress address);
        Task DeleteAddress(string applicantId, string addressId);

        Task<IApplicant> AddEmailAddress(string applicantId, IEmailAddress emailAddress);
        Task UpdateEmailAddress(string applicantId, string emailAddressId, IEmailAddress emailAddress);
        Task DeleteEmailAddress(string applicantId, string emailAddressId);

        Task<IApplicant> AddPhoneNumber(string applicantId,IPhoneNumber phoneNumber);
        Task UpdatePhoneNumber(string applicantId, string phoneId, IPhoneNumber phoneNumber);
        Task DeletePhoneNumber(string applicantId, string phoneId);

        Task<IApplicant> AddEmployment(string applicantId, IEmploymentDetail employmentDetail);
        Task UpdateEmployment(string applicantId, string employmentId, IEmploymentDetail employmentDetail);
        Task DeleteEmployment(string applicantId, string employmentId);

        Task<IApplicant> AddBank(string applicantId, IBankInformation bankDetail);
        Task UpdateBank(string applicantId, string bankId, IBankInformation bankDetail);
        Task DeleteBank(string applicantId, string bankId);
        Task<IApplicant> UpdateApplicant(string applicantId, IApplicant updateApplicantRequest);

        Task UpdateEducation(string applicantId, IEducationInformation educationDetail);

        Task EnableReapplyInCoolOff(string applicantId);
        Task SwitchOffReApply(string applicantId);

        Task UpdateAadhaarNumber(string applicantId, string adhaarnumber);
    }
}