﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CreditExchange.Applicant
{
    public interface IBankInformation
    {
        string BankId { get; set; }
        string BankName { get; set; }
        string AccountNumber { get; set; }
        string AccountHolderName { get; set; }
        string IfscCode { get; set; }
        AccountType AccountType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress,Address>))]
        IAddress BankAddresses { get; set; }
    }
}