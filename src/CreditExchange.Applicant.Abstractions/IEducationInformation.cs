﻿namespace CreditExchange.Applicant
{
    public interface IEducationInformation
    {
        string LevelOfEducation { get; set; }
        string EducationalInstitution { get; set; }
    }
}
