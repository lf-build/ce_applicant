﻿using System.Collections.Generic;
using Newtonsoft.Json;

using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Applicant
{
    public interface IEmploymentDetail
    {
        string EmploymentId { get; set; }
        string Name { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IAddress, Address>))]
        List<IAddress> Addresses { get; set; }

        string Designation { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPhoneNumber, PhoneNumber>))]
        List<IPhoneNumber> WorkPhoneNumbers { get; set; }

        string WorkEmail { get; set; }

        int LengthOfEmploymentInMonths { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IIncomeInformation, IncomeInformation>))]
        IIncomeInformation IncomeInformation { get; set; }

        EmploymentStatus EmploymentStatus { get; set; }

        TimeBucket AsOfDate { get; set; }
        string CinNumber { get; set; }
    }
}
