﻿namespace CreditExchange.Applicant
{
    public interface IIncomeInformation
    {
        double Income { get; set; }
        PaymentFrequency PaymentFrequency { get; set; }
    }
    
}
