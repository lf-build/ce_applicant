﻿namespace CreditExchange.Applicant
{
    public interface IPhoneNumber
    {
        string PhoneId { get; set; }
        string Phone { get; set; }
        string CountryCode { get; set; }
        PhoneType PhoneType { get; set; }
        bool IsDefault { get; set; }
    }
}