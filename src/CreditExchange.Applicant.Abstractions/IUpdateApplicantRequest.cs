﻿using LendFoundry.Foundation.Date;
using System;

namespace CreditExchange.Applicant
{
    public interface IUpdateApplicantRequest
    {
        string AadhaarNumber { get; set; }
        DateTimeOffset DateOfBirth { get; set; }
        string EmailAddress { get; set; }
        string FirstName { get; set; }
        Gender Gender { get; set; }
        string LastName { get; set; }
        MaritalStatus MaritalStatus { get; set; }
        string MiddleName { get; set; }
        string PermanentAccountNumber { get; set; }
        string Salutation { get; set; }
        EmploymentStatus EmploymentStatus { get; set; }
    }
}