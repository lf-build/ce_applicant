﻿namespace CreditExchange.Applicant
{
    public class IncomeInformation : IIncomeInformation
    {
        public double Income { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
    }
}
