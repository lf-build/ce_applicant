﻿namespace CreditExchange.Applicant
{
    public class PhoneNumber : IPhoneNumber
    {
        public string PhoneId { get; set; }
        public string Phone { get; set; }
        public string CountryCode { get; set; }
        public PhoneType PhoneType { get; set; }
        public bool IsDefault { get; set; }
    }
}
