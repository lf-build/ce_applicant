﻿namespace CreditExchange.Applicant
{
    public enum PhoneType
    {
        Residence = 1,
        Mobile = 2,
        Work = 3,
        Alternate = 4
    }
}   
