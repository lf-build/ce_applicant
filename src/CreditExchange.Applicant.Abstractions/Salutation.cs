﻿
namespace CreditExchange.Applicant
{
    public enum Salutation
    {
        Mr,
        Mrs,
        Miss,
        Ms,
        Dr,
        Prof
    }
}
