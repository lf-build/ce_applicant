﻿using System;
using System.Runtime.Serialization;

namespace CreditExchange.Applicant
{
    [Serializable]
    public class UnableToCreateUserException : Exception
    {
        public UnableToCreateUserException()
        {
        }

        public UnableToCreateUserException(string message) : base(message)
        {
        }

        public UnableToCreateUserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnableToCreateUserException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}