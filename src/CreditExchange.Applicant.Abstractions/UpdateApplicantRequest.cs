﻿using LendFoundry.Foundation.Date;
using System;

namespace CreditExchange.Applicant
{
    public class UpdateApplicantRequest : IUpdateApplicantRequest
    {
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string AadhaarNumber { get; set; }
        public string PermanentAccountNumber { get; set; }
        public Gender Gender { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public EmploymentStatus EmploymentStatus { get; set; }

    }
}
