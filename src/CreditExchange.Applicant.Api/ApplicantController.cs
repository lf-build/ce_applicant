﻿using System;
using System.IO;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;
#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace CreditExchange.Applicant.Api
{
    [Route("/")]
    public class ApplicantController : ExtendedController
    {
        #region Constructors
        public ApplicantController(IApplicantService applicantService)
        {
            if (applicantService == null)
                throw new ArgumentNullException(nameof(applicantService));

            ApplicantService = applicantService;

        }

        #endregion

        #region Private Properties
        private IApplicantService ApplicantService { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();
        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicantRequest"></param>
        /// <returns></returns>
        [HttpPost]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> Add([FromBody]ApplicantRequest applicantRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ApplicantService.Add(applicantRequest));
                }   
                //catch (UsernameAlreadyExists exception)
                //{
                //    return new ErrorResult(409, exception.Message);
                //}
                catch (UnableToCreateUserException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpGet("{applicantId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> Get(string applicantId)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.Get(applicantId)));
        }

        [HttpPut("{applicantId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateApplicant(string applicantId, [FromBody]UpdateApplicantRequest applicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.UpdateApplicant(applicantId, applicationRequest)));
        }

        [HttpPut("updateapplicant/{applicantId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateApplicantDetails(string applicantId, [FromBody]Applicant applicationRequest)
        {
            return await ExecuteAsync(async () => Ok(await ApplicantService.UpdateApplicant(applicantId, applicationRequest)));
        }

        [HttpDelete("{applicantId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> Delete(string applicantId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.Delete(applicantId); return NoContentResult; });
        }


        [HttpPost("{applicantId}/addresses")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddAddresses(string applicantId, [FromBody]Address address)
        {
            return await ExecuteAsync((async () => Ok(await ApplicantService.AddAddress(applicantId,address))));
        }
        [HttpPut("{applicantId}/addresses/{addressId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateAddresses(string applicantId,string addressId, [FromBody]Address address)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdateAddress(applicantId,addressId,address); return NoContentResult; });
        }

        [HttpDelete("{applicantId}/addresses/{addressId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DeleteAddresses(string applicantId, string addressId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.DeleteAddress(applicantId, addressId); return NoContentResult; });
        }

        [HttpPost("{applicantId}/emailaddresses")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddEmailAddress(string applicantId, [FromBody]EmailAddress emailAddress)
        {
            return await ExecuteAsync((async () => Ok(await ApplicantService.AddEmailAddress(applicantId, emailAddress))));
        }

        [HttpPut("{applicantid}/emailaddresses/{emailAddressId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateEmailAddress(string applicantId, string emailAddressId, [FromBody]EmailAddress emailAddress)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdateEmailAddress(applicantId, emailAddressId, emailAddress); return NoContentResult; });
        }

        [HttpDelete("{applicantid}/emailaddresses/{emailAddressId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DeleteEmailAddress(string applicantId, string emailAddressId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.DeleteEmailAddress(applicantId, emailAddressId); return NoContentResult; });
        }

        [HttpPost("{applicantId}/phonenumbers")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddPhoneNumber(string applicantId, [FromBody]PhoneNumber phoneNumber)
        {
            return await ExecuteAsync((async () => Ok(await ApplicantService.AddPhoneNumber(applicantId, phoneNumber))));
        }

        [HttpPut("{applicantId}/phonenumbers/{phoneId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdatePhoneNumber(string applicantId, string phoneId, [FromBody]PhoneNumber phoneNumber)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdatePhoneNumber(applicantId, phoneId, phoneNumber); return NoContentResult; });
        }

        [HttpDelete("{applicantId}/phonenumbers/{phoneId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DeletePhoneNumber(string applicantId, string phoneId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.DeletePhoneNumber(applicantId, phoneId); return NoContentResult; });
        }

        [HttpPost("{applicantId}/employments")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddEmployment(string applicantId, [FromBody]EmploymentDetail employment)
        {
            return await ExecuteAsync((async () => Ok(await ApplicantService.AddEmployment(applicantId, employment))));
        }

        [HttpPut("{applicantId}/employments/{employmentId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateEmployment(string applicantId, string employmentId, [FromBody]EmploymentDetail employment)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdateEmployment(applicantId, employmentId, employment); return NoContentResult; });
        }

        [HttpDelete("{applicantId}/employments/{employmentId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DeleteEmployment(string applicantId, string employmentId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.DeleteEmployment(applicantId, employmentId); return NoContentResult; });
        }
        [HttpPost("{applicantId}/banks")]
#if DOTNET2
        [ProducesResponseType(typeof(IApplicant), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AddBank(string applicantId, [FromBody]BankInformation bank)
        {
            return await ExecuteAsync((async () => Ok(await ApplicantService.AddBank(applicantId, bank))));
        }

        [HttpPut("{applicantId}/banks/{bankId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateBank(string applicantId, string bankId, [FromBody]BankInformation bank)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdateBank(applicantId, bankId, bank); return NoContentResult; });
        }

        [HttpDelete("{applicantId}/banks/{bankId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> DeleteBank(string applicantId, string bankId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.DeleteBank(applicantId, bankId); return NoContentResult; });
        }

        [HttpPut("/{applicantId}/associate/user/{userId}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> AssociateUser(string applicantId, string userId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    { await ApplicantService.AssociateUser(applicantId, userId); return NoContentResult; }
                }
                catch (UserAlreadyAssociatedException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }
            });
        }

        [HttpPut("{applicantId}/education")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateEducationInformation(string applicantId, [FromBody]EducationInformation education)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdateEducation(applicantId, education); return NoContentResult; });
        }

        [HttpPost("{applicantId}/EnableReApply")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> EnableReapplyInCoolOff(string applicantId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.EnableReapplyInCoolOff(applicantId); return NoContentResult; });
        }

        [HttpPost("{applicantId}/SwitchOffReApply")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> SwitchOffReApply(string applicantId)
        {
            return await ExecuteAsync(async () => { await ApplicantService.SwitchOffReApply(applicantId); return NoContentResult; });
        }

        [HttpPut("aadhaar/{applicantId}/{aadhaar}")]
#if DOTNET2
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> UpdateAdhaarNumber(string applicantId, string aadhaar)
        {
            return await ExecuteAsync(async () => { await ApplicantService.UpdateAadhaarNumber(applicantId,aadhaar); return NoContentResult; });
        }
#endregion
    }
}


