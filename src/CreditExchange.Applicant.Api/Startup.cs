﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.EventHub.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Security.Identity.Client;
using CreditExchange.Applicant.Persistence;
using LendFoundry.Configuration;

namespace CreditExchange.Applicant.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env) { }
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator , defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Applicant"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CreditExchange.Applicant.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<ApplicantConfigurations>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddIdentityService(Settings.Identity.Host, Settings.Identity.Port);
            //services.AddNumberGeneratorService(Settings.NumberGenerator.Host, Settings.NumberGenerator.Port);

            services.AddTransient<IApplicantConfigurations, ApplicantConfigurations>();
            services.AddTransient<IApplicantConfigurations>(p => p.GetService<IConfigurationService<ApplicantConfigurations>>().Get());
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IApplicantRepository, ApplicantRepository>();
            services.AddTransient<IApplicantService, ApplicantService>();
            //services.AddSwaggerGen();
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Applicant Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            //app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMvc();           
        }
    }

    //public class RequestLoggingMiddleware
    //{
    //    private RequestDelegate Next { get; }

    //    public RequestLoggingMiddleware(RequestDelegate next)
    //    {
    //        Next = next;
    //    }

    //    public async Task Invoke(HttpContext context)
    //    {
    //        // The MVC middleware reads the body stream before we do, which
    //        // means that the body stream's cursor would be positioned at the end.
    //        // We need to reset the body stream cursor position to zero in order
    //        // to read it. As the default body stream implementation does not
    //        // support seeking, we need to explicitly enable rewinding of the stream:
    //        try
    //        {
    //            await Next.Invoke(context);
    //        }
    //        catch (Exception exception)
    //        {

    //        }
    //    }
    //}
}

