﻿using System.Threading.Tasks;
using RestSharp;
using LendFoundry.Foundation.Client;
using System;

namespace CreditExchange.Applicant.Client
{
    public class ApplicantService : IApplicantService
    {
        public ApplicantService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IApplicant> Add(IApplicantRequest applicantRequest)
        {
            var request = new RestRequest("/", Method.POST);
            request.AddJsonBody(applicantRequest);
            return await Client.ExecuteAsync<Applicant>(request);
        }
        public async Task AssociateUser(string applicantId, string userId)
        {
            var request = new RestRequest("{applicantId}/associate/user/{userId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("userId", userId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> Get(string applicantId)
        {
            var request = new RestRequest("{applicantId}", Method.GET);
            request.AddUrlSegment("applicantId", applicantId);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task Delete(string applicantId)
        {
            var request = new RestRequest("{applicantId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IApplicant updateApplicantRequest)
        {
            var request = new RestRequest("updateapplicant/{applicantId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(updateApplicantRequest);
            return await Client.ExecuteAsync<Applicant>(request);
        }
        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest applicantRequest)
        {
            var request = new RestRequest("{applicantId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(applicantRequest);
            return await Client.ExecuteAsync<Applicant>(request);
        }
        public async Task<IApplicant> AddAddress(string applicantId, IAddress address)
        {
            var request = new RestRequest("{applicantId}/addresses", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(address);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task UpdateAddress(string applicantId, string addressId, IAddress address)
        {
            var request = new RestRequest("{applicantId}/addresses/{addressId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("addressId", addressId);
            request.AddJsonBody(address);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteAddress(string applicantId, string addressId)
        {
            var request = new RestRequest("{applicantId}/addresses/{addressId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("addressId", addressId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> AddEmailAddress(string applicantId, IEmailAddress emailAddress)
        {
            var request = new RestRequest("{applicantId}/emailaddresses", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(emailAddress);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task UpdateEmailAddress(string applicantId, string emailAddressId, IEmailAddress emailAddress)
        {
            var request = new RestRequest("{applicantid}/emailaddresses/{emailAddressId}", Method.PUT);
            request.AddUrlSegment("applicantid", applicantId);
            request.AddUrlSegment("emailAddressId", emailAddressId);
            request.AddJsonBody(emailAddress);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteEmailAddress(string applicantId, string emailAddressId)
        {
            var request = new RestRequest("{applicantid}/emailaddresses/{emailAddressId}", Method.DELETE);
            request.AddUrlSegment("applicantid", applicantId);
            request.AddUrlSegment("emailAddressId", emailAddressId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> AddPhoneNumber(string applicantId, IPhoneNumber phoneNumber)
        {
            var request = new RestRequest("{applicantId}/phonenumbers", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(phoneNumber);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task UpdatePhoneNumber(string applicantId, string phoneId, IPhoneNumber phoneNumber)
        {
            var request = new RestRequest("{applicantId}/phonenumbers/{phoneId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("phoneId", phoneId);
            request.AddJsonBody(phoneNumber);
            await Client.ExecuteAsync(request);
        }

        public async Task DeletePhoneNumber(string applicantId, string phoneId)
        {
            var request = new RestRequest("{applicantId}/phonenumbers/{phoneId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("phoneId", phoneId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> AddEmployment(string applicantId, IEmploymentDetail employmentDetail)
        {
            var request = new RestRequest("{applicantId}/employments", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(employmentDetail);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task UpdateEmployment(string applicantId, string employmentId, IEmploymentDetail employmentDetail)
        {
            var request = new RestRequest("{applicantId}/employments/{employmentId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("employmentId", employmentId);
            request.AddJsonBody(employmentDetail);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteEmployment(string applicantId, string employmentId)
        {
            var request = new RestRequest("{applicantId}/employments/{employmentId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("employmentId", employmentId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IApplicant> AddBank(string applicantId, IBankInformation bankDetail)
        {
            var request = new RestRequest("{applicantId}/banks", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(bankDetail);
            return await Client.ExecuteAsync<Applicant>(request);
        }

        public async Task UpdateBank(string applicantId, string bankId, IBankInformation bankDetail)
        {
            var request = new RestRequest("{applicantId}/banks/{bankId}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("bankId", bankId);
            request.AddJsonBody(bankDetail);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteBank(string applicantId, string bankId)
        {
            var request = new RestRequest("{applicantId}/banks/{bankId}", Method.DELETE);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("bankId", bankId);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateEducation(string applicantId, IEducationInformation educationDetail)
        {
            var request = new RestRequest("{applicantId}/education", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddJsonBody(educationDetail);
            await Client.ExecuteAsync(request);
        }
        public async Task UpdateAadhaarNumber(string applicantId, string aadhaar)
        {
            var request = new RestRequest("aadhaar/{applicantId}/{aadhaar}", Method.PUT);
            request.AddUrlSegment("applicantId", applicantId);
            request.AddUrlSegment("aadhaar", aadhaar);
            await Client.ExecuteAsync(request);
        }
        public async Task EnableReapplyInCoolOff(string applicantId)
        {
            var request = new RestRequest("{applicantId}/EnableReApply", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);
            await Client.ExecuteAsync(request);
        }

        public async Task SwitchOffReApply(string applicantId)
        {
            var request = new RestRequest("{applicantId}/SwitchOffReApply", Method.POST);
            request.AddUrlSegment("applicantId", applicantId);

            await Client.ExecuteAsync(request);
        }
    }
}