﻿#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using LendFoundry.Security.Tokens;

namespace CreditExchange.Applicant.Client
{
    public static class ApplicantServiceClientExtensions
    {
        public static IServiceCollection AddApplicantService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicantServiceClientFactory>(p => new ApplicantServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicantServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
