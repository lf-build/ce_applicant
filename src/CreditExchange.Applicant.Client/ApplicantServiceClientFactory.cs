﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

namespace CreditExchange.Applicant.Client
{
    public class ApplicantServiceClientFactory : IApplicantServiceClientFactory
    {
        public ApplicantServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }


        public IApplicantService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ApplicantService(client);
        }

    }
}
