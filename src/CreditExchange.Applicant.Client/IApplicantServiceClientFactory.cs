﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.Applicant.Client
{
    public interface IApplicantServiceClientFactory
    {
        IApplicantService Create(ITokenReader reader);
    }
}
