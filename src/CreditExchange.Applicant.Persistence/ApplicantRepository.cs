﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace CreditExchange.Applicant.Persistence
{
    public class ApplicantRepository : MongoRepository<IApplicant, Applicant>, IApplicantRepository
    {
        static ApplicantRepository ()
        {
            BsonClassMap.RegisterClassMap<Applicant> (map =>
            {
                map.AutoMap ();

                map.MapProperty (a => a.Gender).SetSerializer (new EnumSerializer<Gender> (BsonType.String));
                map.MapProperty (a => a.MaritalStatus).SetSerializer (new EnumSerializer<MaritalStatus> (BsonType.String));
                map.MapProperty (a => a.Addresses).SetIgnoreIfDefault (true);
                map.MapProperty (a => a.PhoneNumbers).SetIgnoreIfDefault (true);
                map.MapProperty (a => a.EmailAddresses).SetIgnoreIfDefault (true);
                map.MapProperty (a => a.BankInformation).SetIgnoreIfDefault (true);
                map.MapProperty (a => a.EmploymentDetails).SetIgnoreIfDefault (true);
                map.MapProperty (a => a.DateOfBirth).SetSerializer (new DateTimeOffsetSerializer (BsonType.Document));
                var type = typeof (Applicant);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<TimeBucket> (map =>
            {
                map.AutoMap ();
                map.MapMember (m => m.Time).SetSerializer (new DateTimeOffsetSerializer (BsonType.Document));

                var type = typeof (TimeBucket);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass (false);
            });

            BsonClassMap.RegisterClassMap<Address> (map =>
            {
                map.AutoMap ();
                var type = typeof (Address);
                map.MapProperty (p => p.AddressType).SetSerializer (new EnumSerializer<AddressType> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<PhoneNumber> (map =>
            {
                map.AutoMap ();
                var type = typeof (PhoneNumber);
                map.MapProperty (p => p.PhoneType).SetSerializer (new EnumSerializer<PhoneType> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<EmailAddress> (map =>
            {
                map.AutoMap ();
                var type = typeof (EmailAddress);
                map.MapProperty (p => p.EmailType).SetSerializer (new EnumSerializer<EmailType> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<IncomeInformation> (map =>
            {
                map.AutoMap ();
                var type = typeof (IncomeInformation);
                map.MapProperty (p => p.PaymentFrequency).SetSerializer (new EnumSerializer<PaymentFrequency> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<BankInformation> (map =>
            {
                map.AutoMap ();
                var type = typeof (BankInformation);
                map.MapProperty (p => p.AccountType).SetSerializer (new EnumSerializer<AccountType> (BsonType.String));
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
            BsonClassMap.RegisterClassMap<EmploymentDetail> (map =>
            {
                map.AutoMap ();
                var type = typeof (EmploymentDetail);
                map.MapProperty (p => p.EmploymentStatus).SetSerializer (new EnumSerializer<EmploymentStatus> (BsonType.String));
                map.MapProperty (a => a.IncomeInformation).SetIgnoreIfDefault (true);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<EducationInformation> (map =>
            {
                map.AutoMap ();
                var type = typeof (EducationInformation);
                map.SetDiscriminator ($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public ApplicantRepository (LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration) : base (tenantService, configuration, "applicants")
        {
            CreateIndexIfNotExists ("applicantId", Builders<IApplicant>.IndexKeys.Ascending (i => i.TenantId).Ascending (i => i.Id));
        }

        public async Task<IApplicant> GetByApplicantId (string applicantId)
        {
            ObjectId obj;
            if (!ObjectId.TryParse (applicantId, out obj))
                return null;
            return await Collection.Find (GetByApplicantIdFilter (applicantId)).FirstOrDefaultAsync ();
        }

        public async Task AssociateUser (string applicantId, string userId)
        {
            var applicant = await GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            if (!string.IsNullOrEmpty (applicant.UserId))
            {
                throw new UserAlreadyAssociatedException ($"Applicant {applicantId} already has associated user");
            }

            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.UserId, userId));
        }

        public async Task UpdateAddress (string applicantId, List<IAddress> addresses)
        {
            await Collection.UpdateOneAsync (
                GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.Addresses, addresses));
        }

        public async Task UpdateEmailAddress (string applicantId, List<IEmailAddress> emailAddress)
        {
            await Collection.UpdateOneAsync (
                GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.EmailAddresses, emailAddress));
        }

        public async Task UpdatePhoneNumber (string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.PhoneNumbers, phoneNumbers));
        }

        public async Task UpdateEmployment (string applicantId, List<IEmploymentDetail> employmentDetails)
        {
            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.EmploymentDetails, employmentDetails));
        }

        public async Task UpdateBanks (string applicantId, List<IBankInformation> banks)
        {
            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.BankInformation, banks));
        }

        public async Task<IApplicant> UpdateApplicant (string applicantId, IApplicant updateApplicantRequest)
        {
            var applicant = await GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");
            updateApplicantRequest.TenantId = applicant.TenantId;
            updateApplicantRequest.UserId = applicant.UserId;
            updateApplicantRequest.PermanentAccountNumber = updateApplicantRequest.PermanentAccountNumber?.ToUpper ();

            applicant.Salutation = updateApplicantRequest.Salutation;
            applicant.FirstName = updateApplicantRequest.FirstName;
            applicant.MiddleName = updateApplicantRequest.MiddleName;
            applicant.LastName = updateApplicantRequest.LastName;
            applicant.AadhaarNumber = updateApplicantRequest.AadhaarNumber;
            applicant.PermanentAccountNumber = updateApplicantRequest.PermanentAccountNumber?.ToUpper ();
            applicant.DateOfBirth = updateApplicantRequest.DateOfBirth;
            applicant.Gender = updateApplicantRequest.Gender;
            applicant.MaritalStatus = updateApplicantRequest.MaritalStatus;
            applicant.Addresses = updateApplicantRequest.Addresses;
            applicant.BankInformation = updateApplicantRequest.BankInformation;
            applicant.EmailAddresses = updateApplicantRequest.EmailAddresses;
            applicant.EmploymentDetails = updateApplicantRequest.EmploymentDetails;
            applicant.HighestEducationInformation = updateApplicantRequest.HighestEducationInformation;
            applicant.IncomeInformation = updateApplicantRequest.IncomeInformation;
            applicant.PhoneNumbers = updateApplicantRequest.PhoneNumbers;

            await Collection.ReplaceOneAsync (GetByApplicantIdFilter (applicantId), updateApplicantRequest);
            //await Collection.UpdateOneAsync(GetByApplicantIdFilter(applicantId),
            //    Builders<IApplicant>.Update.Set(a => a.Salutation, updateApplicantRequest.Salutation)
            //    .Set(a => a.FirstName, updateApplicantRequest.FirstName)
            //    .Set(a => a.MiddleName, updateApplicantRequest.MiddleName)
            //    .Set(a => a.LastName, updateApplicantRequest.LastName)
            //    .Set(a => a.AadhaarNumber, updateApplicantRequest.AadhaarNumber)
            //    .Set(a => a.PermanentAccountNumber, updateApplicantRequest.PermanentAccountNumber)
            //    .Set(a => a.DateOfBirth, updateApplicantRequest.DateOfBirth)
            //    .Set(a => a.Gender, updateApplicantRequest.Gender)
            //    .Set(a => a.MaritalStatus, updateApplicantRequest.MaritalStatus)
            //    .Set(a => a.Addresses, updateApplicantRequest.Addresses)
            //    .Set(a => a.BankInformation, updateApplicantRequest.BankInformation)
            //    .Set(a => a.EmailAddresses, updateApplicantRequest.EmailAddresses)
            //    .Set(a => a.EmploymentDetails, updateApplicantRequest.EmploymentDetails)
            //    .Set(a => a.HighestEducationInformation, updateApplicantRequest.HighestEducationInformation)
            //    .Set(a => a.IncomeInformation, updateApplicantRequest.IncomeInformation)
            //    .Set(a => a.PhoneNumbers, updateApplicantRequest.PhoneNumbers));
            return applicant;
        }
        public async Task<IApplicant> UpdateApplicant (string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {
            var applicant = await GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            applicant.Salutation = updateApplicantRequest.Salutation;
            applicant.FirstName = updateApplicantRequest.FirstName;
            applicant.MiddleName = updateApplicantRequest.MiddleName;
            applicant.LastName = updateApplicantRequest.LastName;
            applicant.AadhaarNumber = updateApplicantRequest.AadhaarNumber;
            applicant.PermanentAccountNumber = updateApplicantRequest.PermanentAccountNumber?.ToUpper ();
            applicant.DateOfBirth = updateApplicantRequest.DateOfBirth;
            applicant.Gender = updateApplicantRequest.Gender;
            applicant.MaritalStatus = updateApplicantRequest.MaritalStatus;

            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.Salutation, updateApplicantRequest.Salutation)
                .Set (a => a.FirstName, updateApplicantRequest.FirstName)
                .Set (a => a.MiddleName, updateApplicantRequest.MiddleName)
                .Set (a => a.LastName, updateApplicantRequest.LastName)
                .Set (a => a.AadhaarNumber, updateApplicantRequest.AadhaarNumber)
                .Set (a => a.PermanentAccountNumber, updateApplicantRequest.PermanentAccountNumber?.ToUpper ())
                .Set (a => a.DateOfBirth, updateApplicantRequest.DateOfBirth)
                .Set (a => a.Gender, updateApplicantRequest.Gender)
                .Set (a => a.MaritalStatus, updateApplicantRequest.MaritalStatus));
            return applicant;
        }

        public async Task UpdateEducation (string applicantId, IEducationInformation educationDetails)
        {
            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.HighestEducationInformation, educationDetails));
        }

        public async Task UpdateAadhaarNumber (string applicantId, string adhaarnumber)
        {
            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.AadhaarNumber, adhaarnumber));
        }

        private FilterDefinition<IApplicant> GetByApplicantIdFilter (string applicantId)
        {
            return Builders<IApplicant>.Filter.Where (a => a.TenantId == TenantService.Current.Id &&
                a.Id == applicantId);
        }

        public async Task SetReApplyFlag (string applicantId, DateTime? reApplyBy)
        {
            await Collection.UpdateOneAsync (GetByApplicantIdFilter (applicantId),
                Builders<IApplicant>.Update.Set (a => a.CanReapplyBy, reApplyBy));
        }
    }
}