﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

using System.Linq;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Identity;

namespace CreditExchange.Applicant
{
    public class ApplicantService : IApplicantService
    {
        public ApplicantService (
            LendFoundry.Security.Identity.Client.IIdentityService identityService,
            IApplicantRepository applicantRepository,
            IApplicantConfigurations applicantConfiguration,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime)
        {
            ValidateConfiguration (applicantConfiguration);
            ApplicantConfigurations = applicantConfiguration;
            EventHubClient = eventHubClient;
            IdentityService = identityService;
            ApplicantRepository = applicantRepository;
            TenantTime = tenantTime;
        }

        private IApplicantRepository ApplicantRepository { get; }
        private IEventHubClient EventHubClient { get; }
        private LendFoundry.Security.Identity.Client.IIdentityService IdentityService { get; }
        private IApplicantConfigurations ApplicantConfigurations { get; }
        private ITenantTime TenantTime { get; }

        public async Task<IApplicant> Add (IApplicantRequest applicantRequest)
        {
            IApplicant applicantModel = null;
            await Task.Run (async () =>
            {
                if (applicantRequest == null)
                    throw new ArgumentNullException (nameof (applicantRequest));

                //var isValid = EnsureInputIsValid(applicantRequest);

                //if(applicantRequest.DateOfBirth.HasValue)
                applicantRequest.DateOfBirth = applicantRequest.DateOfBirth;

                if (applicantRequest.Addresses != null)
                {
                    foreach (var address in applicantRequest.Addresses)
                    {
                        address.AddressId = string.IsNullOrWhiteSpace (address.AddressId) ? GenerateUniqueId () : address.AddressId;
                    }
                }

                if (applicantRequest.PhoneNumbers != null)
                {
                    foreach (var phone in applicantRequest.PhoneNumbers)
                    {
                        phone.PhoneId = string.IsNullOrWhiteSpace (phone.PhoneId) ? GenerateUniqueId () : phone.PhoneId;
                    }
                }
                if (applicantRequest.EmailAddresses != null)
                {
                    foreach (var email in applicantRequest.EmailAddresses)
                    {
                        email.Id = string.IsNullOrWhiteSpace (email.Id) ? GenerateUniqueId () : email.Id;
                    }
                }
                if (applicantRequest.EmploymentDetails != null && applicantRequest.EmploymentDetails.Any ())
                {
                    foreach (var employment in applicantRequest.EmploymentDetails)
                    {
                        employment.EmploymentId = string.IsNullOrWhiteSpace (employment.EmploymentId) ? GenerateUniqueId () : employment.EmploymentId;
                    }
                }
                if (applicantRequest.BankInformation != null && applicantRequest.BankInformation.Any ())
                {
                    foreach (var bank in applicantRequest.BankInformation)
                    {
                        bank.BankId = string.IsNullOrWhiteSpace (bank.BankId) ? GenerateUniqueId () : bank.BankId;
                        if (bank.BankAddresses != null)
                        {

                            bank.BankAddresses.AddressId = string.IsNullOrWhiteSpace (bank.BankAddresses.AddressId) ? GenerateUniqueId () : bank.BankAddresses.AddressId;

                        }
                    }
                }
                applicantModel = new Applicant (applicantRequest);

                if (!string.IsNullOrWhiteSpace (applicantRequest.UserName) && !string.IsNullOrWhiteSpace (applicantRequest.Password))
                {
                    IUser user = new User
                    {
                        Email = applicantRequest.EmailAddresses[0].Email,
                        Name = $"{applicantRequest.FirstName} {applicantRequest.MiddleName} {applicantRequest.LastName}",
                        Username = applicantRequest.UserName,
                        Password = applicantRequest.Password,
                        Roles = ApplicantConfigurations.DefaultRoles
                    };
                    var userInfo = await IdentityService.CreateUser (user);
                    if (string.IsNullOrEmpty (userInfo.Id))
                        throw new UnableToCreateUserException ();

                    applicantModel.UserId = userInfo.Id;
                }

                ApplicantRepository.Add (applicantModel);

            });

            await EventHubClient.Publish (new ApplicantCreated { Applicant = applicantModel });

            return applicantModel;
        }

        public async Task<IApplicant> Get (string applicantId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            return applicant;
        }

        public async Task Delete (string applicantId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            ApplicantRepository.Remove (applicant);

            await EventHubClient.Publish (new ApplicantDeleted { Applicant = applicant });
        }

        public async Task AssociateUser (string applicantId, string userId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (userId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (userId));

            await ApplicantRepository.AssociateUser (applicantId, userId);

            await EventHubClient.Publish (new UserAssociatedWithApplicant { ApplicantId = applicantId, UserId = userId });
        }

        public async Task<IApplicant> UpdateApplicant (string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentNullException (nameof (applicantId), "Value cannot be null or whitespace.");

            var applicant = await ApplicantRepository.UpdateApplicant (applicantId, updateApplicantRequest);

            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }

        public async Task<IApplicant> UpdateApplicant (string applicantId, IApplicant updateApplicantRequest)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentNullException (nameof (applicantId), "Value cannot be null or whitespace.");

            if (updateApplicantRequest == null)
                throw new ArgumentNullException (nameof (updateApplicantRequest));

            //if (updateApplicantRequest.DateOfBirth.HasValue)
            updateApplicantRequest.DateOfBirth = updateApplicantRequest.DateOfBirth;

            if (updateApplicantRequest.Addresses != null)
            {
                foreach (var address in updateApplicantRequest.Addresses)
                {
                    address.AddressId = string.IsNullOrWhiteSpace (address.AddressId) ? GenerateUniqueId () : address.AddressId;
                }
            }

            if (updateApplicantRequest.PhoneNumbers != null)
            {
                foreach (var phone in updateApplicantRequest.PhoneNumbers)
                {
                    phone.PhoneId = string.IsNullOrWhiteSpace (phone.PhoneId) ? GenerateUniqueId () : phone.PhoneId;
                }
            }
            if (updateApplicantRequest.EmailAddresses != null)
            {
                foreach (var email in updateApplicantRequest.EmailAddresses)
                {
                    email.Id = string.IsNullOrWhiteSpace (email.Id) ? GenerateUniqueId () : email.Id;
                }
            }
            if (updateApplicantRequest.EmploymentDetails != null && updateApplicantRequest.EmploymentDetails.Any ())
            {
                foreach (var employment in updateApplicantRequest.EmploymentDetails)
                {
                    employment.EmploymentId = string.IsNullOrWhiteSpace (employment.EmploymentId) ? GenerateUniqueId () : employment.EmploymentId;
                }
            }
            if (updateApplicantRequest.BankInformation != null && updateApplicantRequest.BankInformation.Any ())
            {
                foreach (var bank in updateApplicantRequest.BankInformation)
                {
                    bank.BankId = string.IsNullOrWhiteSpace (bank.BankId) ? GenerateUniqueId () : bank.BankId;
                }
            }

            var applicant = await ApplicantRepository.UpdateApplicant (applicantId, updateApplicantRequest);
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }

        public async Task<IApplicant> AddAddress (string applicantId, IAddress address)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (address == null)
                throw new ArgumentException ($"{nameof(address)} is mandatory");

            //EnsureInputIsValid(new List<IAddress> { address });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var lstAddress = applicant.Addresses ?? new List<IAddress> ();
            if (address.IsDefault)
                lstAddress.ForEach (a => { a.IsDefault = false; });

            address.AddressId = GenerateUniqueId ();

            lstAddress.Add (address);

            applicant.Addresses = lstAddress;

            await ApplicantRepository.UpdateAddress (applicantId, lstAddress);
            await EventHubClient.Publish (new ApplicantAddressAdded { AddresseAdded = address, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }

        public async Task UpdateAddress (string applicantId, string addressId, IAddress address)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value can not be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (addressId))
                throw new ArgumentException ("Value can not be null or whitespace.", nameof (addressId));

            if (address == null)
                throw new ArgumentException ($"{nameof(address)} is mandatory");

            //EnsureInputIsValid(new List<IAddress> { address });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var addresses = applicant.Addresses ?? new List<IAddress> ();
            if (address.IsDefault)
                addresses.ForEach (a => { a.IsDefault = false; });

            var oldAddress = addresses.FirstOrDefault (adr => adr.AddressId == addressId);
            if (oldAddress == null)
                throw new NotFoundException ($"Address with {addressId} for Applicant {applicantId} not found");

            address.AddressId = addressId;
            addresses = addresses.Select (adr => adr.AddressId == addressId ? address : adr).ToList ();

            applicant.Addresses = addresses;

            await ApplicantRepository.UpdateAddress (applicantId, addresses);
            await EventHubClient.Publish (new ApplicantAddressModified { NewAddresses = address, OldAddresses = oldAddress, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task DeleteAddress (string applicantId, string addressId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (addressId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (addressId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var addresses = applicant.Addresses ?? new List<IAddress> ();
            if (addresses.Any (a => a.AddressId == addressId) == false)
                throw new NotFoundException ($"Address with {addressId} for Applicant {applicantId} not found");
            var DeletedAdderss = addresses.First (a => a.AddressId == addressId);
            if (DeletedAdderss.IsDefault)
                throw new InvalidOperationException ("Default address can not be deleted");

            addresses.Remove (addresses.First (a => a.AddressId == addressId));
            applicant.Addresses = addresses;

            await ApplicantRepository.UpdateAddress (applicantId, addresses);
            await EventHubClient.Publish (new ApplicantAddressDeleted { AddresseDeleted = DeletedAdderss, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task<IApplicant> AddEmailAddress (string applicantId, IEmailAddress emailAddress)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ($"{nameof(applicantId)} is mandantory");

            if (emailAddress == null)
                throw new ArgumentException ($"{nameof(emailAddress)} is mandatory");

            //EnsureInputIsValid(new List<IEmailAddress> { emailAddress });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var emailAddresses = applicant.EmailAddresses ?? new List<IEmailAddress> ();
            if (emailAddress.IsDefault)
                emailAddresses.ForEach (a => { a.IsDefault = false; });

            emailAddress.Id = GenerateUniqueId ();

            emailAddresses.Add (emailAddress);

            applicant.EmailAddresses = emailAddresses;

            await ApplicantRepository.UpdateEmailAddress (applicantId, emailAddresses);
            await EventHubClient.Publish (new ApplicantEmailAdded { EmailAddresseAdded = emailAddress, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }

        public async Task UpdateEmailAddress (string applicantId, string emailAddressId, IEmailAddress emailAddress)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (emailAddressId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (emailAddressId));

            if (emailAddress == null)
                throw new ArgumentException ($"{nameof(emailAddress)} is mandatory");

            //EnsureInputIsValid(new List<IEmailAddress> { emailAddress });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var emailAddresses = applicant.EmailAddresses ?? new List<IEmailAddress> ();

            if (emailAddress.IsDefault)
                emailAddresses.ForEach (a => { a.IsDefault = false; });
            var oldEmailAddresses = emailAddresses.FirstOrDefault (a => a.Id == emailAddressId);
            if (oldEmailAddresses == null)
                throw new NotFoundException ($"Email address with {emailAddressId} for Applicant {applicantId} not found");

            emailAddress.Id = emailAddressId;
            emailAddresses = emailAddresses.Select (email => email.Id == emailAddressId ? emailAddress : email).ToList ();

            applicant.EmailAddresses = emailAddresses;
            await ApplicantRepository.UpdateEmailAddress (applicantId, emailAddresses);
            await EventHubClient.Publish (new ApplicantEmailModified { NewEmailAddress = emailAddress, OldEmailAddress = oldEmailAddresses, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task DeleteEmailAddress (string applicantId, string emailAddressId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (emailAddressId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (emailAddressId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var emails = applicant.EmailAddresses ?? new List<IEmailAddress> ();
            if (emails.Any (a => a.Id == emailAddressId) == false)
                throw new NotFoundException ($"Email address with {emailAddressId} for Applicant {applicantId} not found");

            var emailAddressDeleted = emails.First (a => a.Id == emailAddressId);
            if (emailAddressDeleted.IsDefault)
                throw new InvalidOperationException ("Default email address can not be deleted");

            emails.Remove (emails.First (a => a.Id == emailAddressId));
            applicant.EmailAddresses = emails;

            await ApplicantRepository.UpdateEmailAddress (applicantId, emails);
            await EventHubClient.Publish (new ApplicantEmailDeleted { EmailAddresseDeleted = emailAddressDeleted, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task<IApplicant> AddPhoneNumber (string applicantId, IPhoneNumber phoneNumber)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ($"{nameof(applicantId)} is mandantory");

            if (phoneNumber == null)
                throw new ArgumentException ($"{nameof(phoneNumber)} is mandatory");

            //EnsureInputIsValid(new List<IPhoneNumber> { phoneNumber });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var phoneNumbers = applicant.PhoneNumbers ?? new List<IPhoneNumber> ();
            if (phoneNumber.IsDefault)
                phoneNumbers.ForEach (a => { a.IsDefault = false; });

            phoneNumber.PhoneId = GenerateUniqueId ();

            phoneNumbers.Add (phoneNumber);

            applicant.PhoneNumbers = phoneNumbers;

            await ApplicantRepository.UpdatePhoneNumber (applicantId, phoneNumbers);
            await EventHubClient.Publish (new ApplicantPhoneNumberAdded { PhoneNumberAdded = phoneNumber, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }

        public async Task UpdatePhoneNumber (string applicantId, string phoneId, IPhoneNumber phoneNumber)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (phoneId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (phoneId));

            if (phoneNumber == null)
                throw new ArgumentException ($"{nameof(phoneNumber)} is mandatory");

            //EnsureInputIsValid(new List<IPhoneNumber> { phoneNumber });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var phoneNumbers = applicant.PhoneNumbers ?? new List<IPhoneNumber> ();

            if (phoneNumber.IsDefault)
                phoneNumbers.ForEach (a => { a.IsDefault = false; });

            //var currentAddress = addresses.FirstOrDefault(a => a.AddressId == address.AddressId);
            var oldPhoneNumbers = phoneNumbers.FirstOrDefault (a => a.PhoneId == phoneId);
            if (oldPhoneNumbers == null)
                throw new NotFoundException ($"Phone number  {phoneNumber.Phone} for Applicant {applicantId} not found");

            phoneNumber.PhoneId = phoneId;
            phoneNumbers = phoneNumbers.Select (phone => phone.PhoneId == phoneId ? phoneNumber : phone).ToList ();

            applicant.PhoneNumbers = phoneNumbers;
            await ApplicantRepository.UpdatePhoneNumber (applicantId, phoneNumbers);
            await EventHubClient.Publish (new ApplicantPhoneNumberModified { NewPhoneNumber = phoneNumber, OldPhoneNumber = oldPhoneNumbers, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task DeletePhoneNumber (string applicantId, string phoneId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (phoneId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (phoneId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var phones = applicant.PhoneNumbers ?? new List<IPhoneNumber> ();
            if (phones.Any (p => p.PhoneId == phoneId) == false)
                throw new NotFoundException ($"Phone number with {phoneId} for Applicant {applicantId} not found");

            var PhoneDeleted = phones.First (a => a.PhoneId == phoneId);
            if (PhoneDeleted.IsDefault)
                throw new InvalidOperationException ("Default phone number can not be deleted");

            phones.Remove (phones.First (a => a.PhoneId == phoneId));
            applicant.PhoneNumbers = phones;

            await ApplicantRepository.UpdatePhoneNumber (applicantId, phones);
            await EventHubClient.Publish (new ApplicantPhoneNumberDeleted { PhoneNumberDeleted = PhoneDeleted, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task<IApplicant> AddEmployment (string applicantId, IEmploymentDetail employmentDetail)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ($"{nameof(applicantId)} is mandantory");

            if (employmentDetail == null)
                throw new ArgumentException ($"{nameof(employmentDetail)} is mandatory");

            //EnsureInputIsValid(new List<IEmploymentDetail> { employmentDetail });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var employmentDetails = applicant.EmploymentDetails ?? new List<IEmploymentDetail> ();

            employmentDetail.EmploymentId = GenerateUniqueId ();

            employmentDetails.Add (employmentDetail);

            applicant.EmploymentDetails = employmentDetails;

            await ApplicantRepository.UpdateEmployment (applicantId, employmentDetails);
            await EventHubClient.Publish (new ApplicantEmploymentAdded { EmploymentDetailAdded = employmentDetail, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }
        public async Task UpdateEmployment (string applicantId, string employmentId, IEmploymentDetail employmentDetail)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (employmentId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (employmentId));

            if (employmentDetail == null)
                throw new ArgumentException ($"{nameof(employmentDetail)} is mandatory");

            //EnsureInputIsValid(new List<IEmploymentDetail> { employmentDetail });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var employmentDetails = applicant.EmploymentDetails ?? new List<IEmploymentDetail> ();
            var oldEmploymentDetails = employmentDetails.FirstOrDefault (e => e.EmploymentId == employmentId);
            if (oldEmploymentDetails == null)
                throw new NotFoundException ($"Employment detail with {employmentId} for Applicant {applicantId} not found");

            employmentDetail.EmploymentId = employmentId;
            employmentDetails = employmentDetails.Select (e => e.EmploymentId == employmentId ? employmentDetail : e).ToList ();

            applicant.EmploymentDetails = employmentDetails;
            await ApplicantRepository.UpdateEmployment (applicantId, employmentDetails);
            await EventHubClient.Publish (new ApplicantEmploymentModified { NewEmploymentDetail = employmentDetail, OldEmploymentDetail = oldEmploymentDetails, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task DeleteEmployment (string applicantId, string employmentId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (employmentId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (employmentId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var employmentDetails = applicant.EmploymentDetails ?? new List<IEmploymentDetail> ();

            if (employmentDetails.Any (e => e.EmploymentId == employmentId) == false)
                throw new NotFoundException ($"Employment detail with {employmentId} for Applicant {applicantId} not found");

            var employmentDeleted = employmentDetails.First (employment => employment.EmploymentId == employmentId);
            employmentDetails.Remove (employmentDetails.First (employment => employment.EmploymentId == employmentId));
            applicant.EmploymentDetails = employmentDetails;

            await ApplicantRepository.UpdateEmployment (applicantId, employmentDetails);
            await EventHubClient.Publish (new ApplicantEmploymentDeleted { EmploymentDetailDeleted = employmentDeleted, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }
        public async Task<IApplicant> AddBank (string applicantId, IBankInformation bankDetail)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ($"{nameof(applicantId)} is mandantory");

            if (bankDetail == null)
                throw new ArgumentException ($"{nameof(bankDetail)} is mandatory");

            //EnsureInputIsValid(new List<IBankInformation> { bankDetail });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var banks = applicant.BankInformation ?? new List<IBankInformation> ();

            bankDetail.BankId = string.IsNullOrWhiteSpace (bankDetail.BankId) ? GenerateUniqueId () : bankDetail.BankId;
            if (bankDetail.BankAddresses != null)
            {
                bankDetail.BankAddresses.AddressId = string.IsNullOrWhiteSpace (bankDetail.BankAddresses.AddressId) ? GenerateUniqueId () : bankDetail.BankAddresses.AddressId;
            }
            banks.Add (bankDetail);

            applicant.BankInformation = banks;

            await ApplicantRepository.UpdateBanks (applicantId, banks);
            await EventHubClient.Publish (new ApplicantBankAdded { BankInformationAdded = bankDetail });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
            return applicant;
        }

        public async Task UpdateBank (string applicantId, string bankId, IBankInformation bankDetail)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (bankId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (bankId));

            if (bankDetail == null)
                throw new ArgumentException ($"{nameof(bankDetail)} is mandatory");

            //EnsureInputIsValid(new List<IBankInformation> { bankDetail });

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var banks = applicant.BankInformation ?? new List<IBankInformation> ();
            var oldBankInformation = banks.FirstOrDefault (a => a.BankId == bankId);
            if (oldBankInformation == null)
                throw new NotFoundException ($"Bank with {bankId} for Applicant {applicantId} not found");

            if (bankDetail.BankAddresses != null)
            {
                bankDetail.BankAddresses.AddressId = string.IsNullOrWhiteSpace (bankDetail.BankAddresses.AddressId) ? GenerateUniqueId () : bankDetail.BankAddresses.AddressId;
            }

            bankDetail.BankId = bankId;
            banks = banks.Select (bank => bank.BankId == bankId ? bankDetail : bank).ToList ();

            applicant.BankInformation = banks;
            await ApplicantRepository.UpdateBanks (applicantId, banks);
            await EventHubClient.Publish (new ApplicantBankModified { NewBankInformation = bankDetail, OldBankInformation = oldBankInformation, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task DeleteBank (string applicantId, string bankId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (bankId))
                throw new ArgumentException ("Argument is null or whitespace", nameof (bankId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);
            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");

            var banks = applicant.BankInformation ?? new List<IBankInformation> ();

            if (banks.Any (a => a.BankId == bankId) == false)
                throw new NotFoundException ($"Bank with {bankId} for Applicant {applicantId} not found");

            var DeletedBank = banks.First (a => a.BankId == bankId);
            banks.Remove (banks.First (a => a.BankId == bankId));
            applicant.BankInformation = banks;

            await ApplicantRepository.UpdateBanks (applicantId, banks);
            await EventHubClient.Publish (new ApplicantBankDeleted { BankInformationDeleted = DeletedBank, ApplicantId = applicantId });
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task UpdateEducation (string applicantId, IEducationInformation educationDetail)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (educationDetail == null)
                throw new ArgumentException ($"{nameof(educationDetail)} is mandatory");

            if (string.IsNullOrWhiteSpace (educationDetail.EducationalInstitution))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrWhiteSpace (educationDetail.LevelOfEducation))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            if (applicant == null)
                throw new NotFoundException ($"Applicant {applicantId} not found");
            EducationInformation educationinformation = new EducationInformation ();
            educationinformation.EducationalInstitution = educationDetail.EducationalInstitution;
            educationinformation.LevelOfEducation = educationDetail.LevelOfEducation;

            applicant.HighestEducationInformation = educationinformation;

            await ApplicantRepository.UpdateEducation (applicantId, applicant.HighestEducationInformation);
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task UpdateAadhaarNumber (string applicantId, string adhaarnumber)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            if (string.IsNullOrEmpty (adhaarnumber))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (adhaarnumber));

            var applicant = await ApplicantRepository.GetByApplicantId (applicantId);

            applicant.AadhaarNumber = adhaarnumber;

            await ApplicantRepository.UpdateAadhaarNumber (applicantId, applicant.AadhaarNumber);
            await EventHubClient.Publish (new ApplicantModified { Applicant = applicant });
        }

        public async Task EnableReapplyInCoolOff (string applicantId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            var reApplyBy = TenantTime.Now.Date.AddDays (ApplicantConfigurations.ReApplyExtensionDays);
            await ApplicantRepository.SetReApplyFlag (applicantId, reApplyBy);
        }

        public async Task SwitchOffReApply (string applicantId)
        {
            if (string.IsNullOrWhiteSpace (applicantId))
                throw new ArgumentException ("Value cannot be null or whitespace.", nameof (applicantId));

            await ApplicantRepository.SetReApplyFlag (applicantId, null);
        }

        #region Private Methods - Validations
        private bool EnsureInputIsValid (IApplicantRequest applicantRequest)
        {
            var isValid = true;
            if (applicantRequest == null)
                throw new ArgumentNullException ($"{nameof(applicantRequest)} applicant cannot be empty");

            if (string.IsNullOrWhiteSpace (applicantRequest.FirstName))
                return isValid = false;

            if (string.IsNullOrWhiteSpace (applicantRequest.LastName))
                return isValid = false;

            if (applicantRequest.PhoneNumbers == null || !applicantRequest.PhoneNumbers.Any ())
                return isValid = false;

            foreach (var phone in applicantRequest.PhoneNumbers)
            {
                if (!Enum.IsDefined (typeof (PhoneType), phone.PhoneType))
                    return isValid = false;

                if (phone.PhoneType == PhoneType.Mobile)
                {
                    if (string.IsNullOrWhiteSpace (phone.Phone))
                        return isValid = false;
                }
            }
            return isValid;
        }

        private void EnsureInputIsValid_old (IApplicantRequest applicantRequest)
        {
            if (applicantRequest == null)
                throw new ArgumentNullException ($"{nameof(applicantRequest)} applicant cannot be empty");

            if (string.IsNullOrWhiteSpace (applicantRequest.FirstName))
                throw new ArgumentNullException ($"{nameof(applicantRequest.FirstName)} is mandatory");

            if (string.IsNullOrWhiteSpace (applicantRequest.LastName))
                throw new ArgumentNullException ($"{nameof(applicantRequest.LastName)} is mandatory");

            EnsureInputIsValid (applicantRequest.PhoneNumbers);
            //if (string.IsNullOrEmpty(applicantRequest.UserName) && string.IsNullOrEmpty(applicantRequest.Password))
            //{
            //    if (string.IsNullOrEmpty(applicantRequest.UserId))
            //        throw new ArgumentNullException($"Applicant {nameof(applicantRequest.UserId)} is mandatory");
            //}

            //if (string.IsNullOrEmpty(applicantRequest.UserId))
            //{
            //    if (string.IsNullOrEmpty(applicantRequest.UserName))
            //        throw new ArgumentNullException($"Applicant {nameof(applicantRequest.UserName)} is mandatory");

            //    if (string.IsNullOrEmpty(applicantRequest.Password))
            //        throw new ArgumentNullException($"Applicant {nameof(applicantRequest.Password)} is mandatory");
            //}
            //if (string.IsNullOrWhiteSpace(applicantRequest.Salutation))
            //    throw new ArgumentNullException($"{nameof(applicantRequest.Salutation)} is mandatory");

            //if (applicantRequest.DateOfBirth == null)
            //    throw new ArgumentNullException($"{nameof(applicantRequest.DateOfBirth)} is mandatory");

            //if (!Enum.IsDefined(typeof(MaritalStatus), applicantRequest.MaritalStatus))
            //    throw new InvalidEnumArgumentException(nameof(applicantRequest.MaritalStatus), (int)applicantRequest.MaritalStatus, typeof(MaritalStatus));

            //if (!Enum.IsDefined(typeof(Gender), applicantRequest.Gender))
            //    throw new InvalidEnumArgumentException(nameof(applicantRequest.Gender), (int)applicantRequest.Gender, typeof(Gender));

            //if (string.IsNullOrWhiteSpace(applicantRequest.PermanentAccountNumber))
            //    throw new ArgumentNullException($"{nameof(applicantRequest.PermanentAccountNumber)} is mandatory");

            //if (string.IsNullOrWhiteSpace(applicantRequest.AadhaarNumber))
            //    throw new ArgumentNullException($"{nameof(applicantRequest.AadhaarNumber)} is mandatory");

            //EnsureInputIsValid(applicantRequest.EmailAddresses);
            //EnsureEmployementIsValid(applicantRequest.EmploymentDetails);
            //EnsureInputIsValid(applicantRequest.Addresses);
            //if (applicantRequest.EmploymentDetails != null && applicantRequest.EmploymentDetails.Any())
            //{
            //    
            //}

            //if (applicantRequest.BankInformation != null && applicantRequest.BankInformation.Any())
            //{
            //    EnsureInputIsValid(applicantRequest.BankInformation);
            //}
        }

        private void EnsureInputIsValid (IUpdateApplicantRequest updateApplicantRequest)
        {
            if (updateApplicantRequest == null)
                throw new ArgumentNullException ($"{nameof(updateApplicantRequest)} cannot be empty");

            if (string.IsNullOrWhiteSpace (updateApplicantRequest.FirstName))
                throw new ArgumentNullException ($"{nameof(updateApplicantRequest.FirstName)} is mandatory");

            if (string.IsNullOrWhiteSpace (updateApplicantRequest.LastName))
                throw new ArgumentNullException ($"{nameof(updateApplicantRequest.LastName)} is mandatory");

            if (updateApplicantRequest.DateOfBirth == null)
                throw new ArgumentNullException ($"{nameof(updateApplicantRequest.DateOfBirth)} is mandatory");

            if (!Enum.IsDefined (typeof (MaritalStatus), updateApplicantRequest.MaritalStatus))
                throw new InvalidEnumArgumentException (nameof (updateApplicantRequest.MaritalStatus), (int) updateApplicantRequest.MaritalStatus, typeof (MaritalStatus));

            if (!Enum.IsDefined (typeof (Gender), updateApplicantRequest.Gender))
                throw new InvalidEnumArgumentException (nameof (updateApplicantRequest.Gender), (int) updateApplicantRequest.Gender, typeof (Gender));

            if (string.IsNullOrWhiteSpace (updateApplicantRequest.AadhaarNumber))
                throw new ArgumentNullException ($"{nameof(updateApplicantRequest.AadhaarNumber)} is mandatory");

            if (string.IsNullOrWhiteSpace (updateApplicantRequest.PermanentAccountNumber))
                throw new ArgumentNullException ($"{nameof(updateApplicantRequest.PermanentAccountNumber)} is mandatory");
        }

        private void EnsureInputIsValid (IEnumerable<IBankInformation> banks)
        {
            foreach (var bank in banks)
            {
                if (string.IsNullOrWhiteSpace (bank.BankName))
                    throw new ArgumentNullException ($"{nameof(bank.BankName)} is mandatory");

                if (string.IsNullOrWhiteSpace (bank.AccountHolderName))
                    throw new ArgumentNullException ($"{nameof(bank.AccountHolderName)} is mandatory");

                if (string.IsNullOrWhiteSpace (bank.AccountNumber))
                    throw new ArgumentNullException ($"{nameof(bank.AccountNumber)} is mandatory");

                if (string.IsNullOrWhiteSpace (bank.IfscCode))
                    throw new ArgumentNullException ($"{nameof(bank.IfscCode)} is mandatory");

                if (!Enum.IsDefined (typeof (AccountType), bank.AccountType))
                    throw new InvalidEnumArgumentException ($"{nameof(bank.AccountType)} is not valid");
            }
        }
        private void EnsureInputIsValid (List<IEmailAddress> emailAddresses)
        {
            if (emailAddresses == null || !emailAddresses.Any ())
                throw new ArgumentNullException ($"Applicant {nameof(emailAddresses)} is mandatory");

            foreach (var email in emailAddresses)
            {
                if (string.IsNullOrWhiteSpace (email.Email))
                    throw new ArgumentNullException ($"{nameof(email.Email)} is mandatory");

                if (!Enum.IsDefined (typeof (EmailType), email.EmailType))
                    throw new InvalidEnumArgumentException ($"{nameof(email.EmailType)} is not valid");
            }
        }
        private void EnsureInputIsValid (List<IAddress> addresses)
        {

            foreach (var address in addresses)
            {
                if (string.IsNullOrWhiteSpace (address.AddressLine1))
                    throw new ArgumentNullException ($"Applicant {nameof(address.AddressLine1)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.AddressLine2))
                    throw new ArgumentNullException ($"Applicant {nameof(address.AddressLine2)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Location))
                    throw new ArgumentNullException ($"Applicant {nameof(address.Location)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.City))
                    throw new ArgumentNullException ($"Applicant {nameof(address.City)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.State))
                    throw new ArgumentNullException ($"Applicant {nameof(address.State)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.PinCode))
                    throw new ArgumentNullException ($"Applicant {nameof(address.PinCode)} is mandatory");

                if (string.IsNullOrWhiteSpace (address.Country))
                    throw new ArgumentNullException ($"Applicant {nameof(address.Country)} is mandatory");

                if (!Enum.IsDefined (typeof (AddressType), address.AddressType))
                    throw new InvalidEnumArgumentException ($"{nameof(address.AddressType)} is not valid");
            }
        }
        private void EnsureInputIsValid (IEnumerable<IEmploymentDetail> employmentDetails)
        {
            foreach (var employment in employmentDetails)
            {
                if (string.IsNullOrWhiteSpace (employment.Name))
                    throw new ArgumentNullException ($"{nameof(employment.Name)} is mandatory");

                if (string.IsNullOrWhiteSpace (employment.Designation))
                    throw new ArgumentNullException ($"{nameof(employment.Designation)} is mandatory");

                if (employment.Addresses == null)
                    throw new ArgumentNullException ($"{nameof(employment.Addresses)} is mandatory");

                if (employment.WorkPhoneNumbers == null)
                    throw new ArgumentNullException ($"{nameof(employment.WorkPhoneNumbers)} is mandatory");

                if (employment.WorkEmail == null)
                    throw new ArgumentNullException ($"{nameof(employment.WorkEmail)} is mandatory");

                if (employment.LengthOfEmploymentInMonths <= 0)
                    throw new ArgumentNullException ($"{nameof(employment.LengthOfEmploymentInMonths)} is mandatory");

                if (!Enum.IsDefined (typeof (EmploymentStatus), employment.EmploymentStatus))
                    throw new InvalidEnumArgumentException ($"{nameof(employment.EmploymentStatus)} is not valid");

                employment.Addresses.ForEach (address =>
                {
                    if (string.IsNullOrWhiteSpace (address.AddressLine1))
                        throw new InvalidArgumentException ($"Work address {nameof(address.AddressLine1)} is mandatory");

                    if (string.IsNullOrWhiteSpace (address.City))
                        throw new InvalidArgumentException ($"Work address {nameof(address.City)} is mandatory");

                    if (string.IsNullOrWhiteSpace (address.State))
                        throw new InvalidArgumentException ($"Work address {nameof(address.State)} is mandatory");

                    if (string.IsNullOrWhiteSpace (address.PinCode))
                        throw new InvalidArgumentException ($"Work address {nameof(address.PinCode)} is mandatory");

                    if (string.IsNullOrWhiteSpace (address.Country))
                        throw new ArgumentNullException ($"Work address {nameof(address.Country)} is mandatory");

                    if (!Enum.IsDefined (typeof (AddressType), address.AddressType))
                        throw new InvalidEnumArgumentException ($"{nameof(address.AddressType)} is not valid");
                });

                employment.WorkPhoneNumbers.ForEach (phone =>
                {
                    if (string.IsNullOrWhiteSpace (phone.Phone))
                        throw new InvalidArgumentException ($"Work phone {nameof(phone.Phone)} is mandatory");

                    if (!Enum.IsDefined (typeof (PhoneType), phone.PhoneType))
                        throw new InvalidEnumArgumentException ($"{nameof(phone.PhoneType)} is not valid");
                });

                employment.AsOfDate = new TimeBucket (TenantTime.Today);
            }
        }

        private void EnsureEmployementIsValid (IEnumerable<IEmploymentDetail> employmentDetails)
        {

            foreach (var employment in employmentDetails)
            {
                if (string.IsNullOrWhiteSpace (employment.Name))
                    throw new ArgumentNullException ($"{nameof(employment.Name)} is mandatory");

                if (employment.WorkEmail == null)
                    throw new ArgumentNullException ($"{nameof(employment.WorkEmail)} is mandatory");

                if (!Enum.IsDefined (typeof (EmploymentStatus), employment.EmploymentStatus))
                    throw new InvalidEnumArgumentException ($"{nameof(employment.EmploymentStatus)} is not valid");

            }
        }
        private void EnsureInputIsValid (List<IPhoneNumber> phoneNumbers)
        {
            if (phoneNumbers == null || !phoneNumbers.Any ())
                throw new ArgumentNullException ($"Applicant {nameof(phoneNumbers)} is mandatory");

            foreach (var phone in phoneNumbers)
            {
                if (string.IsNullOrWhiteSpace (phone.Phone))
                    throw new ArgumentNullException ($"Applicant {nameof(phone.Phone)} is mandatory");

                if (!Enum.IsDefined (typeof (PhoneType), phone.PhoneType))
                    throw new InvalidEnumArgumentException ($"{nameof(phone.PhoneType)} is not valid");
            }
        }

        private static string GenerateUniqueId ()
        {
            return Guid.NewGuid ().ToString ("N");
        }

        private static void ValidateConfiguration (IApplicantConfigurations configurations)
        {
            if (configurations == null)
                throw new ArgumentNullException (nameof (configurations));

            if (configurations.DefaultRoles == null || !configurations.DefaultRoles.Any ())
                throw new ArgumentNullException ((nameof (configurations.DefaultRoles)));
        }

        #endregion

    }
}