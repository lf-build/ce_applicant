﻿using System;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using Moq;
using Xunit;

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;
using CreditExchange.Applicant.Fakes;

namespace CreditExchange.Applicant.Api.Tests
{
    public class ApplicantControllerTest
    {
        #region Public Methods
        [Fact]
        public async void AddApplicantReturnApplicantIdOnValidApplicant()
        {
            var applicantService = GetApplicantService();
            var applicantRequest = new ApplicantRequest()
            {
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Female,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Alternate,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,

                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                    }
                }
            }
            };

            var response = (ObjectResult)await GetController(applicantService).Add(applicantRequest);
            Assert.IsType<HttpOkObjectResult>(response);
            var aplicantResponse = ((HttpOkObjectResult)response).Value as IApplicant;

            Assert.NotNull(aplicantResponse);
            Assert.NotNull(aplicantResponse.Id);
            Assert.NotEmpty(aplicantResponse.Id);

        }

        [Fact]
        public async void AddApplicantReturnErrorIfUserAlreadyExists()
        {
            var applicantService = GetApplicantService();
            var applicantRequest = new ApplicantRequest()
            {
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "Test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Female,
                UserName = "TestUserExists",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,

                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }
            };
            var response = (ObjectResult)await GetController(applicantService).Add(applicantRequest);
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(409, result.StatusCode);
        }

        [Fact]
        public async void AssociateUserWithApplicatSuccess()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                        //IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,

                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AssociateUser("Applicant1", "User1");
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void AssociateUserWithApplicatReturnBadRequestWhenUserAlreadyAssociated()
        {

            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = "User1",
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,

                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                  }
                }
              }
            }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = (ObjectResult)await GetController(applicantService).AssociateUser("Applicant1", "User2");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void AssociateUserWithApplicat_ThrowsExceptionWithInvalidInputs()
        {

            var applicants = new List<IApplicant>
            {
                 new Applicant()
                 {
                    Id = "Applicant1",
                    UserId = null,
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                    Addresses = new List<IAddress>
                    {
                        new Address()
                        {
                            AddressLine1 = "test1",
                            AddressLine2 = "test3",
                            City = "Ahmedabad",
                            Country = "India",
                            PinCode = "380034",
                            State = "Gujarat"
                        }
                    },
                    AadhaarNumber = "12345",
                    PermanentAccountNumber = "45345345",
                    DateOfBirth = DateTimeOffset.Now,
                    EmailAddresses = new List<IEmailAddress>
                    {
                        new EmailAddress()
                        {
                            Email = "test@gmail.com",
                            EmailType = EmailType.Personal,
                            IsDefault = true,
                           // IsVerified = false
                        }
                    },
                    Gender = Gender.Female,
                    PhoneNumbers = new List<IPhoneNumber>
                    {
                        new PhoneNumber()
                        {
                            Phone = "9898989898",
                            PhoneType = PhoneType.Residence,
                            CountryCode = "91"
                        }
                    },
                    Salutation = "Miss",
                    MaritalStatus = MaritalStatus.Married,

                    EmploymentDetails = new List<IEmploymentDetail>
                    {
                        new EmploymentDetail()
                        {
                            Addresses = new List<IAddress>
                            {
                                new Address
                                {
                                    AddressLine1 = "Employeertest1",
                                    AddressLine2 = "Employeertest2",
                                    City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                                 }
                            },
                            Designation = "Manager",
                        //EmploymentStatus = EmploymentStatus.Employed,
                            LengthOfEmploymentInMonths = 60,
                            Name = "SIS",
                            WorkEmail = "TestSigma@gmail.com",
                            WorkPhoneNumbers = new List<IPhoneNumber>
                            {
                                new PhoneNumber
                                {
                                    Phone = "9898989898",
                                    PhoneType = PhoneType.Residence,
                                    CountryCode = "91"
                                }
                            }
                        }
                    }
                }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = (ObjectResult)await GetController(applicantService).AssociateUser(null, "User2");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void AssociateUserWithApplicat_ThrowsExceptionWithInvalidUserInput()
        {

            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Mobile,
                    CountryCode = "91"
                }
                },
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,

                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                   }
                }
            }
            }
            };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = (ObjectResult)await GetController(applicantService).AssociateUser("Application1", " ");
            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetBanksToApplicantSuccess()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var banks =
                      new BankInformation()
                      {
                          BankName = "Canara Bank",
                          AccountHolderName = "Sigma Info",
                          AccountNumber = "222222222222",
                          AccountType = AccountType.Savings,
                          IfscCode = "ifsccode111",
                         // IsVerified = false
                      };
            //};

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddBank("Applicant1", banks);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetBanksReturnBadRequestForInvliadData()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var banks =
                      new BankInformation()
                      {
                          BankName = "Canara Bank",
                          AccountHolderName = "Sigma Info",
                          AccountNumber = "222222222222",
                          AccountType = AccountType.Savings,
                          IfscCode = "ifsccode111",
                         // IsVerified = false
                      };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddBank("Applicant1", banks);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetBanksReturnNotFoundForApplicantIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                      //  IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var banks =
                      new BankInformation()
                      {
                          BankName = "Canara Bank",
                          AccountHolderName = "Sigma Info",
                          AccountNumber = "222222222222",
                          AccountType = AccountType.Savings,
                          IfscCode = "ifsccode111",
                         // IsVerified = false
                      };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddBank("Applicant2", banks);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetBanksReturnNotFoundForBankIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var banks =
                      new BankInformation()
                      {
                          BankName = "Canara Bank",
                          AccountHolderName = "Sigma Info",
                          AccountNumber = "222222222222",
                          AccountType = AccountType.Savings,
                          IfscCode = "ifsccode111",
                         // IsVerified = false
                      };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdateBank("Applicant2", "b01", banks);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetAddressesToApplicantSuccess()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var addresses =
                    new Address()
                    {
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                    };



            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddAddresses("Applicant1", addresses);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetAddressesReturnBadRequestForInvliadData()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var addresses =
                     new Address()
                     {
                         AddressId = "02",
                         AddressLine2 = "Address2",
                         City = "Surat",
                         Country = "India",
                         PinCode = "380034",
                         State = "Gujarat",
                         AddressType = AddressType.Permanant
                     };



            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdateAddresses("Applicant1", "02", addresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetAddressesNotFoundForApplicantIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var addresses =
                    new Address()
                    {
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant

                    };


            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddAddresses("App123", addresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetAddressesNotFoundForAddressIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var addresses =
                    new Address()
                    {
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                    };


            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddAddresses("Applicant1", addresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetEmailAddressesToApplicantSuccess()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                      //  IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };


            var emailAddresses =
                     new EmailAddress()
                     {
                         Email = "test1Sigma@gmail.com",
                         EmailType = EmailType.Personal,
                         IsDefault = true,
                     };


            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddEmailAddress("Applicant1", emailAddresses);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetEmailAddressesReturnBadRequestForInvliadData()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                         //   IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var emailAddresses =
                     new EmailAddress()
                     {
                         Email = "test1Sigma@gmail.com",
                         EmailType = EmailType.Personal,
                         IsDefault = true,
                     };


            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddEmailAddress("Applicant1", emailAddresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetEmailAddressesNotFoundForApplicantIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var emailAddresses =
                     new EmailAddress()
                     {
                         Email = "test1Sigma@gmail.com",
                         EmailType = EmailType.Personal,
                         IsDefault = true,
                     };


            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddEmailAddress("Applicant111", emailAddresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetEmailAddressesNotFoundForIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var emailAddresses =
                    new EmailAddress()
                    {
                        Id = "e1101",
                        Email = "testUpdate@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                        //IsVerified = false
                    };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddEmailAddress("Applicant1", emailAddresses);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetEmploymentToApplicantSuccess()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                      //  IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };


            var employment =
                     new EmploymentDetail()
                     {
                         EmploymentId = "e01",
                         Addresses = new List<IAddress>
                        {
                            new Address()
                            {
                                AddressLine1 = "EmployeertestUpdate1",
                                AddressLine2 = "EmployeertestUpdate2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                 AddressType = AddressType.Work

                            }
                        },
                         Designation = "Manager",
                         EmploymentStatus = EmploymentStatus.Salaried,
                         LengthOfEmploymentInMonths = 60,
                         Name = "SigmaInfo",
                         WorkEmail = "TestSigma@gmail.com",
                         WorkPhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber()
                            {
                                Phone = "9898989898",
                                PhoneType = PhoneType.Work,
                                CountryCode = "91"
                             }
                        }
                     };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdateEmployment("Applicant1", "e01", employment);

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetEmploymentReturnBadRequestForInvliadData()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                      //  IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };


            var employment =
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                 AddressType = AddressType.Work
                            }
                        },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com",
                        WorkPhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber
                            {
                                Phone = "9898989898",
                                PhoneType = PhoneType.Work,
                                CountryCode = "91"
                             }
                        }
                    };
            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddEmployment("Applicant1", employment);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetEmploymentNotFoundForApplicantIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };


            var employment =
                     new EmploymentDetail()
                     {
                         EmploymentId = "e01",
                         Addresses = new List<IAddress>
                        {
                            new Address()
                            {
                                AddressLine1 = "EmployeertestUpdate1",
                                AddressLine2 = "EmployeertestUpdate2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                 AddressType = AddressType.Work

                            }
                        },
                         Designation = "Manager",
                         EmploymentStatus = EmploymentStatus.Salaried,
                         LengthOfEmploymentInMonths = 60,
                         Name = "SigmaInfo",
                         WorkEmail = "TestSigma@gmail.com",
                         WorkPhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber()
                            {
                                Phone = "9898989898",
                                PhoneType = PhoneType.Work,
                                CountryCode = "91"
                             }
                        }
                     };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdateEmployment("Applicant111", "e01", employment);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetEmploymentNotFoundForEmploymentIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                      //  IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                          //  IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };


            var employment =
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                 AddressType = AddressType.Work
                            }
                        },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com",
                        WorkPhoneNumbers = new List<IPhoneNumber>
                        {
                            new PhoneNumber
                            {
                                Phone = "9898989898",
                                PhoneType = PhoneType.Work,
                                CountryCode = "91"
                             }
                        }
                    };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddEmployment("Applicant1", employment);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetPhoneNumbersToApplicantSuccess()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        PhoneId = "02",
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var phoneNumbers =
                        new PhoneNumber
                        {
                            Phone = "978821232123",
                            PhoneType = PhoneType.Mobile,
                            CountryCode = "01"
                        };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddPhoneNumber("Applicant1", phoneNumbers);
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async void SetPhoneNumbersReturnBadRequestForInvliadData()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        PhoneId = "02",
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var phoneNumbers =
                        new PhoneNumber
                        {
                            PhoneId = "02",
                            PhoneType = PhoneType.Residence,
                            CountryCode = "91"
                        };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).UpdatePhoneNumber("Applicant1", "PhoneId", phoneNumbers);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public async void SetPhoneNumbersNotFoundForApplicantIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        PhoneId = "02",
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111"
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                    Country = "India",
                                    PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var phoneNumbers =
                        new PhoneNumber
                        {
                            Phone = "978821232123",
                            PhoneType = PhoneType.Mobile,
                            CountryCode = "01"
                        };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddPhoneNumber("Applicant123", phoneNumbers);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public async void SetPhoneNumbersNotFoundForEmploymentIdNotExists()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                 {
                   Id = "Applicant1",
                   UserId = null,
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat"
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                      //  IsVerified = false
                    }
                },
                Gender = Gender.Female,
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                    {
                        PhoneId = "02",
                        Phone = "9898989898",
                        PhoneType = PhoneType.Residence,
                        CountryCode = "91"
                    }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                BankInformation = new List<IBankInformation>
                    {
                     new BankInformation()
                        {
                            BankId = "b01",
                            BankName = "HDFC Bank",
                            AccountHolderName = "Sigma Info Solutions",
                            AccountNumber = "1212121212121212",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111",
                           // IsVerified = false
                        }
                    },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                PinCode = "380034"
                            }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType =PhoneType.Work,
                        CountryCode = "91"
                      }
                    }
                }
              }
            }
            };

            var phoneNumbers = new PhoneNumber
            {
                PhoneId = "022",
                Phone = "978812121212",
                PhoneType = PhoneType.Residence,
                CountryCode = "91"
            };

            var fakeApplicantRepository = GetApplicantRepository(applicants);
            var applicantService = GetApplicantService(fakeApplicantRepository);
            var response = await GetController(applicantService).AddPhoneNumber("Applicant1", phoneNumbers);

            Assert.IsType<ErrorResult>(response);
            var result = response as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(404, result.StatusCode);
        }

        #endregion

        #region Private Methods
        private ApplicantController GetController(IApplicantService applicantService)
        {
            return new ApplicantController(applicantService);
        }
        private static IApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
        }
        private static IApplicantService GetApplicantService(IApplicantRepository fakeApplicantRepository, IEnumerable<IApplicant> applicants = null)
        {
            return new ApplicantService(Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>(), fakeApplicantRepository, new ApplicantConfigurations() { DefaultRoles = new string[] { "Applicant" } }, Mock.Of<IEventHubClient>(), Mock.Of<ITenantTime>());
        }
        private static IApplicantRepository GetApplicantRepository(IEnumerable<IApplicant> applicants = null)
        {
            return new FakeApplicantRepository(Mock.Of<ITenantTime>(), applicants);
        }
        #endregion
    }
}
