﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using Xunit;
using Moq;

using LendFoundry.Foundation.Services;

namespace CreditExchange.Applicant.Client.Tests
{
    public class ApplicantServiceClientTests
    {
        #region Constructors
        public ApplicantServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ApplicantServiceClient = new ApplicantService(MockServiceClient.Object);
        }
        #endregion

        #region Public Methods
        [Fact]
        public async void Client_Add_Applicant()
        {
            var applicantRequest = new ApplicantRequest()
            {
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },

                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "Test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true,
                       // IsVerified = false
                    }
                },
                Gender = Gender.Male,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Mobile,
                    CountryCode = "91"
                }
                },
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,

                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
              }
            };
            MockServiceClient.Setup(s => s.ExecuteAsync<Applicant>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r)
               .Returns(
                   () =>
                       Task.FromResult(
                           new Applicant()
                           ));

            var result = await ApplicantServiceClient.Add(applicantRequest);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);

        }
        [Fact]
        public async void Client_AssociateUser()
        {
            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
             .Callback<IRestRequest>(r => Request = r)
             .ReturnsAsync(true);

            await ApplicantServiceClient.AssociateUser("ApplicantId", "UserId");
            Assert.Equal("/{applicantId}/associate/user/{userId}", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }
        #endregion

        #region Private Methods
        private ApplicantService ApplicantServiceClient { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> MockServiceClient { get; }
        #endregion

    }
}
