﻿using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using CreditExchange.Applicant.Fakes;

using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;
using System.ComponentModel;

namespace CreditExchange.Applicant.Tests
{
    public class ApplicantServiceTests
    {
        #region Public Methods
        [Fact]
        public async void ApplicantService_Add_ThrowExceptionWhenRequestIsNull()
        {
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(null));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenFirstNameIsNullOrEmpty()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = new ApplicantRequest()
            {
                FirstName = "",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Female,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Mobile,
                    CountryCode = "91"
                }
                },
                Salutation = "Miss",
                MaritalStatus = MaritalStatus.Married,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }
            };
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenDateOfBirthIsNull()
        {
            // var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = new ApplicantRequest()
            {
                FirstName = "",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "Test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Residence,
                    CountryCode = "91"
                }
                },
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Single,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }
            };
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenAddressStateIsNull()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = new ApplicantRequest()
            {
                FirstName = "",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },

                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                DateOfBirth = DateTimeOffset.Now,
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "Test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                UserName = "TestLogin",
                Password = "TestPassword",
                PhoneNumbers = new List<IPhoneNumber>
                {
                    new PhoneNumber()
                {
                      Phone = "9898989898",
                    PhoneType = PhoneType.Mobile,
                    CountryCode = "91"
                }
                },
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Single,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }
            };
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_Add_ThowExceptionWhenPhoneNumberIsNull()
        {
            //var fakeApplicantService = GetApplicantService();
            var fakeApplicantRepository = GetFakeApplicantRepository();
            var service = GetApplicantService(fakeApplicantRepository);

            var applicant = new ApplicantRequest()
            {
                FirstName = "",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                UserName = "TestLogin",
                Password = "TestPassword",
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }
            };
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.Add(applicant));
        }

        [Fact]
        public async void ApplicantService_AssociateUser()
        {

            var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "Applicant1",
                FirstName = "FirstName",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }}};
            //var fakeApplicantService = GetApplicantService()
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await service.AssociateUser("Applicant1", "User1");
        }

        [Fact]
        public async void ApplicantService_AssociateUser_ThrowsExceptionWhenUserIsAlreadyAssociated()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                Id = "Applicant1",
                UserId = "User4",
                FirstName = "FirstName",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }}};
            //var fakeApplicantService = GetApplicantService()
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<UserAlreadyAssociatedException>(async () => await service.AssociateUser("Applicant1", "User1"));
        }

        [Fact]
        public async void ApplicantService_AssociateUser_ThrowsExceptionWithInvalidAruguments()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                Id = "Applicant1",
                UserId = "User4",
                FirstName = "FirstName",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com",
                    WorkPhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    }
                    }
                }
            }}};
            //var fakeApplicantService = GetApplicantService()
            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("Applicant1", ""));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("", "User1"));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("Applicant5", ""));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser(null, "User1"));
            await Assert.ThrowsAsync<ArgumentException>(async () => await service.AssociateUser("Applicant1", null));
        }

        [Fact]
        public async void ApplicantService_SetPhoneNumbers()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        PhoneId = "02",
                          Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com"

               }
            }
            } };

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var phoneNumbers =
                    new PhoneNumber
                    {
                        Phone = "978821232123",
                        PhoneType = PhoneType.Mobile,
                        CountryCode = "01"
                    };

            await service.AddPhoneNumber("applicant1", phoneNumbers);
            Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().PhoneNumbers.Count);
            Assert.Equal(PhoneType.Residence, fakeApplicantRepository.Applicants.ToList().First().PhoneNumbers.Where(p => p.PhoneId == "02").ToList().FirstOrDefault().PhoneType);
        }

        [Fact]
        public async void ApplicantService_SetPhoneNumbers_ThrowsWhenInputsAreInvalid()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat"
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        PhoneId = "02",
                          Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com"

               }
            }
            } };

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var phoneNumbers = new PhoneNumber
            {
                //PhoneId = "02",
                Phone = "978812121212",
                PhoneType = PhoneType.Residence,
                CountryCode = "91"
            };

            await Assert.ThrowsAsync<ArgumentException>(async () => await service.UpdatePhoneNumber(null, "02", phoneNumbers));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdatePhoneNumber("Applicant1", "02", null));
            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdatePhoneNumber("Applicant1", "02", phoneNumbers));
        }

        [Fact]
        public async void ApplicantService_SetAddresses()
        {
            var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                         AddressId = "01",
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "Address1",
                    AddressLine2 = "Address2",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Current
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        PhoneId = "02",
                          Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com"

               }
            }
            } };

            var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
            var service = GetApplicantService(fakeApplicantRepository);

            var addresses =
                     new Address()
                     {
                         AddressLine1 = "AddressUpdate1",
                         AddressLine2 = "Address2",
                         City = "Surat",
                         Country = "India",
                         PinCode = "380034",
                         State = "Gujarat",
                         AddressType = AddressType.Permanant
                     };

        await service.AddAddress("applicant1", addresses);
        Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().Addresses.Count);
            Assert.Equal(AddressType.Permanant, fakeApplicantRepository.Applicants.ToList().First().Addresses.Where(p => p.AddressId == "02").ToList().FirstOrDefault().AddressType);
        }

    [Fact]
    public async void ApplicantService_SetAddresses_ThrowsWhenInputsAreInvalid()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                Addresses = new List<IAddress>
                {
                    new Address()
                    {
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "Address1",
                    AddressLine2 = "Address2",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Current
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation ="Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        PhoneId = "02",
                          Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com"

               }
            } } };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        var addresses =
                 new Address()
                 {
                     AddressId = "02",
                     AddressLine2 = "Address2",
                     AddressType = AddressType.Current
                 };

        await Assert.ThrowsAsync<ArgumentException>(async () => await service.AddAddress(null, addresses));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddAddress("Applicant1", null));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddAddress("Applicant1", addresses));
    }

    [Fact]
    public async void ApplicantService_SetEmailAddresses()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                PinCode = "380034"
                            }
                            },
                        Designation = "Manager",
                        //EmploymentStatus = EmploymentStatus.Employed,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                }
            }
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        var emailAddresses = new EmailAddress()
        {
            Email = "testUpdate@gmail.com",
            EmailType = EmailType.Personal,
            IsDefault = true
        };

        await service.AddEmailAddress("applicant1", emailAddresses);
        Assert.Equal(1, fakeApplicantRepository.Applicants.ToList().First().EmailAddresses.Count);
        Assert.Equal("testUpdate@gmail.com", fakeApplicantRepository.Applicants.ToList().First().EmailAddresses.Where(p => p.Id == "e01").ToList().FirstOrDefault().Email);
    }

    [Fact]
    public async void ApplicantService_SetEmailAddresses_ThrowsWhenInputsAreInvalid()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                FirstName = "Test",
                LastName = "Test",
                MiddleName = "T",
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                         AddressId = "01",
                    AddressLine1 = "test1",
                    AddressLine2 = "test3",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                         AddressId = "02",
                    AddressLine1 = "Address1",
                    AddressLine2 = "Address2",
                    City = "Ahmedabad",
                    Country = "India",
                    PinCode = "380034",
                    State = "Gujarat",
                    AddressType = AddressType.Current
                }

                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    { new PhoneNumber
                    {
                          Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                     {
                        PhoneId = "02",
                          Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                    Addresses = new List<IAddress>
                        {
                            new Address {  AddressLine1 = "Employeertest1",
                AddressLine2 = "Employeertest2",
                City = "Ahmedabad",
                Country = "India",
                PinCode = "380034" }
                        },
                    Designation = "Manager",
                    //EmploymentStatus = EmploymentStatus.Employed,
                    LengthOfEmploymentInMonths = 60,
                    Name = "SIS",
                    WorkEmail = "TestSigma@gmail.com"

               }
            }
            } };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        var emailAddresses = 
                     new EmailAddress()
                    {
                       Email = "test1Sigma@gmail.com",
                       EmailType = EmailType.Personal,
                       IsDefault = true,
                       Id = ""
                     };


            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddEmailAddress(null, emailAddresses));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddEmailAddress("Applicant1", null));
        await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.AddEmailAddress("Applicant1", emailAddresses));
    }

    [Fact]
    public async void ApplicantService_SetEmployment()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                }
            }
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

            var employment =
                        new EmploymentDetail()
                        {
                            Addresses = new List<IAddress>
                            {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                 AddressType = AddressType.Work
                            }
                            },
                            Designation = "Manager",
                            EmploymentStatus = EmploymentStatus.Salaried,
                            LengthOfEmploymentInMonths = 60,
                            Name = "SIS",
                            WorkEmail = "TestSigma@gmail.com",
                            WorkPhoneNumbers = new List<IPhoneNumber>
                            {
                            new PhoneNumber
                            {
                                Phone = "9898989898",
                                PhoneType = PhoneType.Work,
                                CountryCode = "91"
                             }
                            }
                        };
        await service.AddEmployment("applicant1", employment);
        Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().EmploymentDetails.Count);
        Assert.Equal("SigmaInfo", fakeApplicantRepository.Applicants.ToList().First().EmploymentDetails.Where(e => e.EmploymentId == "e01").First().Name);
    }

    [Fact]
    public async void ApplicantService_SetEmployment_ThrowsWhenInputsAreInvalid()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                    Addresses = new List<IAddress>
                    {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                 },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                        },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                }
            }
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

            var employment =
                        new EmploymentDetail()
                        {
                            Addresses = new List<IAddress>
                            {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                 AddressType = AddressType.Work
                            }
                            },
                            Designation = "Manager",
                            EmploymentStatus = EmploymentStatus.Salaried,
                            LengthOfEmploymentInMonths = 60,
                            Name = "SIS",
                            WorkEmail = "TestSigma@gmail.com",
                            WorkPhoneNumbers = new List<IPhoneNumber>
                            {
                            new PhoneNumber
                            {
                                Phone = "9898989898",
                                PhoneType = PhoneType.Work,
                                CountryCode = "91"
                             }
                            }
                        };
        await Assert.ThrowsAsync<ArgumentException>(async () => await service.AddEmployment(null, employment));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddEmployment("Applicant1", null));
        await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.AddEmployment("Applicant1", employment));
    }


    [Fact]
    public async void ApplicantService_SetBanks()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                     new BankInformation()
                    {
                        BankId = "b01",
                        BankName = "HDFC",
                        AccountHolderName = "Sigma",
                        AccountNumber = "1111111111111111",
                        AccountType = AccountType.Savings,
                        IfscCode = "ifsccode111"
                    }
                }
            }
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        var banks = 
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountHolderName = "Sigma Info",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111"
                      };

            await service.AddBank("applicant1", banks);
        Assert.Equal(2, fakeApplicantRepository.Applicants.ToList().First().BankInformation.Count);
        Assert.Equal("Sigma Info Solutions", fakeApplicantRepository.Applicants.ToList().First().BankInformation.Where(e => e.BankId == "b01").First().AccountHolderName);
        Assert.Equal("1212121212121212", fakeApplicantRepository.Applicants.ToList().First().BankInformation.Where(e => e.BankId == "b01").First().AccountNumber);
        Assert.Equal("HDFC Bank", fakeApplicantRepository.Applicants.ToList().First().BankInformation.Where(e => e.BankId == "b01").First().BankName);
    }

    [Fact]
    public async void ApplicantService_SetBanks_ThrowsWhenInputsAreInvalid()
    {

        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },
                AadhaarNumber = "12345",
                PermanentAccountNumber = "45345345",
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },
                Gender = Gender.Male,
                Salutation = "Mr.",
                MaritalStatus = MaritalStatus.Married,
                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                     new BankInformation()
                    {
                        BankName = "HDFC",
                        AccountHolderName = "Sigma",
                        AccountNumber = "1111111111111111",
                        AccountType = AccountType.Savings,
                        IfscCode = "ifsccode111"
                    }
                }
            }
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        var banks = 
                      new BankInformation()
                        {
                            BankName = "Canara Bank",
                            AccountNumber = "222222222222",
                            AccountType = AccountType.Savings,
                            IfscCode = "ifsccode111"
                      };

            await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddBank(null, banks));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddBank("Applicant1", null));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.AddBank("Applicant1", banks));
    }


    [Fact]
    public async void ApplicantService_UpdateApplicant()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                    AadhaarNumber = "12345",
                    PermanentAccountNumber = "45345345",
                    Gender = Gender.Male,
                    Salutation = "Mr.",
                    MaritalStatus = MaritalStatus.Married,
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },

                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },

                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                     new BankInformation()
                    {
                        BankId = "b01",
                        BankName = "HDFC",
                        AccountHolderName = "Sigma",
                        AccountNumber = "1111111111111111",
                        AccountType = AccountType.Savings,
                        IfscCode = "ifsccode111"
                    }
                }
            }
        };

        var updateApplicantRequest = new UpdateApplicantRequest()
        {
            FirstName = "TestUpdate",
            LastName = "Test",
            MiddleName = "T",
            AadhaarNumber = "12345Update",
            PermanentAccountNumber = "45345345Update",
            Gender = Gender.Male,
            Salutation = "Mr.",
            MaritalStatus = MaritalStatus.Single
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        await service.UpdateApplicant("applicant1", updateApplicantRequest);
        Assert.Equal("12345Update", fakeApplicantRepository.Applicants.ToList().First().AadhaarNumber);
        Assert.Equal("45345345Update", fakeApplicantRepository.Applicants.ToList().First().PermanentAccountNumber);
        Assert.Equal(MaritalStatus.Single, fakeApplicantRepository.Applicants.ToList().First().MaritalStatus);
    }

    [Fact]
    public async void ApplicantService_UpdateApplicant_ThrowsWhenInputsAreInvalid()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                    AadhaarNumber = "12345",
                    PermanentAccountNumber = "45345345",
                    Gender = Gender.Male,
                    Salutation = "Mr.",
                    MaritalStatus = MaritalStatus.Married,
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },

                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },

                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                     new BankInformation()
                    {
                        BankId = "b01",
                        BankName = "HDFC",
                        AccountHolderName = "Sigma",
                        AccountNumber = "1111111111111111",
                        AccountType = AccountType.Savings,
                        IfscCode = "ifsccode111"
                    }
                }
            }
        };

        var updateApplicantRequest = new UpdateApplicantRequest()
        {
            LastName = "Test",
            MiddleName = "T",
            AadhaarNumber = "12345Update",
            PermanentAccountNumber = "45345345Update",
            Gender = Gender.Male,
            Salutation = "Mr."
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant(null, updateApplicantRequest));
       // await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("Applicant1", null));
        await Assert.ThrowsAsync<ArgumentNullException>(async () => await service.UpdateApplicant("Applicant1", updateApplicantRequest));
        updateApplicantRequest.FirstName = "TestUpdate";
        await Assert.ThrowsAsync<InvalidEnumArgumentException>(async () => await service.UpdateApplicant("Applicant1", updateApplicantRequest));
    }

    [Fact]
    public async void ApplicantService_GetApplicantById()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                    AadhaarNumber = "12345",
                    PermanentAccountNumber = "45345345",
                    Gender = Gender.Male,
                    Salutation = "Mr.",
                    MaritalStatus = MaritalStatus.Married,
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },
                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },

                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                     new BankInformation()
                    {
                        BankId = "b01",
                        BankName = "HDFC",
                        AccountHolderName = "Sigma",
                        AccountNumber = "1111111111111111",
                        AccountType = AccountType.Savings,
                        IfscCode = "ifsccode111"
                    }
                }
            }
        };

        var updateApplicantRequest = new UpdateApplicantRequest()
        {
            FirstName = "TestUpdate",
            LastName = "Test",
            MiddleName = "T",
            AadhaarNumber = "12345Update",
            PermanentAccountNumber = "45345345Update",
            Gender = Gender.Male,
            Salutation = "Mr.",
            MaritalStatus = MaritalStatus.Single
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        await service.Get("applicant1");
        Assert.Equal("applicant1", fakeApplicantRepository.Applicants.ToList().First().Id);
        Assert.Equal("45345345", fakeApplicantRepository.Applicants.ToList().First().PermanentAccountNumber); ;
    }

    [Fact]
    public async void ApplicantService_GetApplicantById_ThrowsWhenInputsAreInvalid()
    {
        var applicants = new List<IApplicant>
            {
                new Applicant()
                {
                    Id = "applicant1",
                    FirstName = "Test",
                    LastName = "Test",
                    MiddleName = "T",
                    AadhaarNumber = "12345",
                    PermanentAccountNumber = "45345345",
                    Gender = Gender.Male,
                    Salutation = "Mr.",
                    MaritalStatus = MaritalStatus.Married,
                 Addresses = new List<IAddress>
                {
                    new Address()
                    {
                        AddressId = "01",
                        AddressLine1 = "test1",
                        AddressLine2 = "test3",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Permanant
                },
                     new Address()
                    {
                        AddressId = "02",
                        AddressLine1 = "Address1",
                        AddressLine2 = "Address2",
                        City = "Ahmedabad",
                        Country = "India",
                        PinCode = "380034",
                        State = "Gujarat",
                        AddressType = AddressType.Current
                    }
                },

                EmailAddresses = new List<IEmailAddress>
                {
                    new EmailAddress()
                    {
                        Id = "e01",
                        Email = "test@gmail.com",
                        EmailType = EmailType.Personal,
                        IsDefault = true
                    }
                },

                PhoneNumbers = new List<IPhoneNumber>
                    {
                    new PhoneNumber
                    {
                        Phone = "9898989898",
                        PhoneType = PhoneType.Work,
                        CountryCode = "91"
                    },
                    new PhoneNumber
                    {
                        PhoneId = "02",
                        Phone = "978812121212",
                        PhoneType = PhoneType.Alternate,
                        CountryCode = "91"
                    }
                 },
                EmploymentDetails = new List<IEmploymentDetail>
                {
                    new EmploymentDetail()
                    {
                        EmploymentId = "e01",
                        Addresses = new List<IAddress>
                        {
                            new Address
                            {
                                AddressLine1 = "Employeertest1",
                                AddressLine2 = "Employeertest2",
                                City = "Ahmedabad",
                                Country = "India",
                                State = "Gujarat",
                                PinCode = "380034",
                                AddressType = AddressType.Work
                            }
                            },
                        Designation = "Manager",
                        EmploymentStatus = EmploymentStatus.Salaried,
                        LengthOfEmploymentInMonths = 60,
                        Name = "SIS",
                        WorkEmail = "TestSigma@gmail.com"
                    }
                },
                BankInformation = new List<IBankInformation>()
                {
                     new BankInformation()
                    {
                        BankId = "b01",
                        BankName = "HDFC",
                        AccountHolderName = "Sigma",
                        AccountNumber = "1111111111111111",
                        AccountType = AccountType.Savings,
                        IfscCode = "ifsccode111"
                    }
                }
            }
        };

        var updateApplicantRequest = new UpdateApplicantRequest()
        {
            FirstName = "TestUpdate",
            LastName = "Test",
            MiddleName = "T",
            AadhaarNumber = "12345Update",
            PermanentAccountNumber = "45345345Update",
            Gender = Gender.Male,
            Salutation = "Mr.",
            MaritalStatus = MaritalStatus.Single
        };

        var fakeApplicantRepository = GetFakeApplicantRepository(applicants);
        var service = GetApplicantService(fakeApplicantRepository);

        await Assert.ThrowsAsync<ArgumentException>(async () => await service.Get(null));
        await Assert.ThrowsAsync<NotFoundException>(async () => await service.Get("applicant2"));
    }
    #endregion

    #region Private Methods
    private static FakeApplicantService GetApplicantService(IEnumerable<IApplicant> applicants = null)
    {
        return new FakeApplicantService(new UtcTenantTime(), applicants ?? new List<IApplicant>());
    }

    private static FakeApplicantRepository GetFakeApplicantRepository(List<IApplicant> applicants = null)
    {
        return new FakeApplicantRepository(new UtcTenantTime(), applicants ?? new List<IApplicant>());
    }

    private static IApplicantService GetApplicantService(IApplicantRepository fakeApplicantRepository)
    {
        return new ApplicantService(Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>(), fakeApplicantRepository, new ApplicantConfigurations() { DefaultRoles = new string[] { "Applicant" } }, Mock.Of<IEventHubClient>(), Mock.Of<ITenantTime>());
    }
    #endregion
}
}
