﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;

namespace CreditExchange.Applicant.Fakes
{
    public class FakeApplicantRepository : IApplicantRepository
    {
        #region Private Properties
        private ITenantTime TenantTime { get; set; }
        public List<IApplicant> Applicants { get; } = new List<IApplicant>();
        #endregion

        #region Public Methods
        public FakeApplicantRepository(ITenantTime tenantTime, IEnumerable<IApplicant> applicants) : this(tenantTime)
        {
            Applicants.AddRange(applicants);
        }
        public FakeApplicantRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public async Task<IApplicant> GetByApplicantId(string applicantId)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");
                return applicantData;
            });
        }

        public async Task<IApplicant> UpdateApplicant(string applicantId, IUpdateApplicantRequest updateApplicantRequest)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                applicantData.Salutation = updateApplicantRequest.Salutation;
                applicantData.FirstName = updateApplicantRequest.FirstName;
                applicantData.MiddleName = updateApplicantRequest.MiddleName;
                applicantData.LastName = updateApplicantRequest.LastName;
                applicantData.AadhaarNumber = updateApplicantRequest.AadhaarNumber;
                applicantData.PermanentAccountNumber = updateApplicantRequest.PermanentAccountNumber;
                applicantData.DateOfBirth = updateApplicantRequest.DateOfBirth;
                applicantData.Gender = updateApplicantRequest.Gender;
                applicantData.MaritalStatus = updateApplicantRequest.MaritalStatus;


                return applicantData;
            });
        }

        public Task<IApplicant> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IApplicant>> All(Expression<Func<IApplicant, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IApplicant item)
        {
            throw new NotImplementedException();
        }

        public void Remove(IApplicant item)
        {
            throw new NotImplementedException();
        }

        public void Update(IApplicant item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IApplicant, bool>> query)
        {
            throw new NotImplementedException();
        }

        public async Task AssociateUser(string applicantId, string userId)
        {
            await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                if (!string.IsNullOrEmpty(applicantData.UserId))
                {
                    throw new UserAlreadyAssociatedException($"Applicant {applicantId} already has associated user");
                }
                applicantData.UserId = userId;
            });
        }

        public async Task<IApplicant> SetAddresses(string applicantId, List<IAddress> addresses)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                var lstAddress = applicantData.Addresses ?? new List<IAddress>();

                foreach (var address in addresses)
                {
                    if (string.IsNullOrWhiteSpace(address.AddressId))
                    {
                        address.AddressId = Guid.NewGuid().ToString("N");
                    }
                    else
                    {
                        var currentAddress = lstAddress.FirstOrDefault(a => a.AddressId == address.AddressId);
                        if (currentAddress == null)
                            throw new NotFoundException($"Address {address.AddressId} for Applicant {applicantId} not found");
                        lstAddress.Remove(currentAddress);
                    }
                    lstAddress.Add(address);
                }
                applicantData.Addresses = lstAddress;
                return applicantData;
            });
        }

    public async Task<IApplicant> SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                var lstPhoneNumber = applicantData.PhoneNumbers ?? new List<IPhoneNumber>();

                foreach (var phone in phoneNumbers)
                {
                    if (string.IsNullOrWhiteSpace(phone.PhoneId))
                    {
                        phone.PhoneId = Guid.NewGuid().ToString("N");
                    }
                    else
                    {
                        var currentPhone = lstPhoneNumber.FirstOrDefault(a => a.PhoneId == phone.PhoneId);
                        if (currentPhone == null)
                            throw new NotFoundException($"Phone Number {phone.Phone} for Applicant {applicantId} not found");
                        lstPhoneNumber.Remove(currentPhone);
                    }
                    lstPhoneNumber.Add(phone);
                }
                applicantData.PhoneNumbers = lstPhoneNumber;
                return applicantData;
            });
        }

        public async Task<IApplicant> SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                var lstEmailAddress = applicantData.EmailAddresses ?? new List<IEmailAddress>();

                foreach (var email in emailAddresses)
                {
                    if (string.IsNullOrWhiteSpace(email.Id))
                    {
                        email.Id = Guid.NewGuid().ToString("N");
                    }
                    else
                    {
                        var currentEmail = lstEmailAddress.FirstOrDefault(a => a.Id == email.Id);
                        if (currentEmail == null)
                            throw new NotFoundException($"Email Address {email.Email} for Applicant {applicantId} not found");
                        lstEmailAddress.Remove(currentEmail);
                    }
                    lstEmailAddress.Add(email);
                }
                applicantData.EmailAddresses = lstEmailAddress;
                return applicantData;
            });
        }

        public async Task<IApplicant> SetEmployment(string applicantId, List<IEmploymentDetail> employmentDetails)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                var lstEmployment = applicantData.EmploymentDetails ?? new List<IEmploymentDetail>();

                foreach (var employment in employmentDetails)
                {
                    if (string.IsNullOrWhiteSpace(employment.EmploymentId))
                    {
                        employment.EmploymentId = Guid.NewGuid().ToString("N");
                    }
                    else
                    {
                        var currentEmployment = lstEmployment.FirstOrDefault(e => e.EmploymentId == employment.EmploymentId);
                        if (currentEmployment == null)
                            throw new NotFoundException($"Email Address {employment.EmploymentId} for Applicant {applicantId} not found");
                        lstEmployment.Remove(currentEmployment);
                    }
                    lstEmployment.Add(employment);
                }
                applicantData.EmploymentDetails = lstEmployment;
                return applicantData;
            });
        }

        public async Task<IApplicant> SetBanks(string applicantId, List<IBankInformation> banks)
        {
            return await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                var lstBank = applicantData.BankInformation ?? new List<IBankInformation>();

                foreach (var bank in banks)
                {
                    if (string.IsNullOrWhiteSpace(bank.BankId))
                    {
                        bank.BankId = Guid.NewGuid().ToString("N");
                    }
                    else
                    {
                        var currentBank = lstBank.FirstOrDefault(b => b.BankId == bank.BankId);
                        if (currentBank == null)
                            throw new NotFoundException($"Email Address {currentBank.BankId} for Applicant {applicantId} not found");
                        lstBank.Remove(currentBank);
                    }
                    lstBank.Add(bank);
                }
                applicantData.BankInformation = lstBank;
                return applicantData;
            });
        }

        public Task UpdateAddress(string applicantId, List<IAddress> addresses)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEmailAddress(string applicantId, List<IEmailAddress> emailAddresses)
        {
            throw new NotImplementedException();
        }

        public Task UpdatePhoneNumber(string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEmployment(string applicantId, List<IEmploymentDetail> employmentDetails)
        {
            throw new NotImplementedException();
        }

        public Task UpdateBanks(string applicantId, List<IBankInformation> banks)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> UpdateApplicant(string applicantId, IApplicant updateApplicantRequest)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEducation(string applicantId, IEducationInformation educationDetails)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
