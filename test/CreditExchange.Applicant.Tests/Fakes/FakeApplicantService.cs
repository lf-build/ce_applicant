﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;

namespace CreditExchange.Applicant.Fakes
{
    public class FakeApplicantService : IApplicantService
    {
        #region Private Properties
        private ITenantTime TenantTime { get; set; }
        private List<IApplicant> Applicants { get; } = new List<IApplicant>();
        private int CurrentId { get; set; }
        #endregion

        #region Constructors
        public FakeApplicantService(ITenantTime tenantTime, IEnumerable<IApplicant> applicants) : this(tenantTime)
        {
            Applicants.AddRange(applicants);
            CurrentId = Applicants.Count() + 1;
        }
        public FakeApplicantService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
            CurrentId = 1;
        }
        #endregion

        #region Public Methods
        public Task<IApplicant> Add(IApplicantRequest applicantRequest)
        {

            if (!string.IsNullOrWhiteSpace(applicantRequest.UserName) && !string.IsNullOrWhiteSpace(applicantRequest.Password))
            {
                IUserInfo userInfo = FakeUserCreater.CreateUser(new LendFoundry.Security.Identity.User()
                {
                    Email = applicantRequest.EmailAddresses[0].Email,
                    Name = applicantRequest.UserName,
                    Password = applicantRequest.Password,
                    PasswordSalt = applicantRequest.Password,
                    Username = applicantRequest.UserName,
                    Roles = new string[] { "VP of Sales" }
                });
            }
            IApplicant applicantResponse = new Applicant(applicantRequest);
            applicantResponse.Id = (new Guid()).ToString();
            return Task.FromResult(applicantResponse);
        }

        public Task<IApplicant> Get(string applicantId)
        {
            throw new NotImplementedException();
        }

        public Task Delete(string applicantId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateApplicant(string applicantNumber, IUpdateApplicantRequest applicantRequest)
        {
            throw new NotImplementedException();
        }

        public async Task AssociateUser(string applicantId, string userId)
        {
            await Task.Run(() =>
            {
                var applicantData = Applicants.Where(applicant => applicant.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                if (!string.IsNullOrEmpty(applicantData.UserId))
                {
                    throw new UserAlreadyAssociatedException($"Applicant {applicantId} already has associated user");
                }
                applicantData.UserId = userId;
            });
        }

        public Task SetAddresses(string applicantId, List<IAddress> addresses)
        {
            throw new NotImplementedException();
        }

        public async Task SetPhoneNumbers(string applicantId, List<IPhoneNumber> phoneNumbers)
        {
            await Task.Run(() =>
            {
                var applicantData = Applicants.Where(a => a.Id == applicantId).FirstOrDefault();

                if (applicantData == null)
                    throw new NotFoundException($"Applicant {applicantId} not found");

                applicantData.PhoneNumbers = phoneNumbers;
            });
        }

        public Task SetEmailAddresses(string applicantId, List<IEmailAddress> emailAddresses)
        {
            throw new NotImplementedException();
        }

        public Task SetEmployment(string applicantId, List<IEmploymentDetail> emailAddresses)
        {
            throw new NotImplementedException();
        }

        public Task SetBanks(string applicantId, List<IBankInformation> banks)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> AddAddress(string applicantId, IAddress address)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAddress(string applicantId, string addressId, IAddress address)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAddress(string applicantId, string addressId)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> AddEmailAddress(string applicantId, IEmailAddress emailAddress)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEmailAddress(string applicantId, string emailAddressId, IEmailAddress emailAddress)
        {
            throw new NotImplementedException();
        }

        public Task DeleteEmailAddress(string applicantId, string emailAddressId)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> AddPhoneNumber(string applicantId, IPhoneNumber phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task UpdatePhoneNumber(string applicantId, string phoneId, IPhoneNumber phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task DeletePhoneNumber(string applicantId, string phoneId)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> AddEmployment(string applicantId, IEmploymentDetail employmentDetail)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEmployment(string applicantId, string employmentId, IEmploymentDetail employmentDetail)
        {
            throw new NotImplementedException();
        }

        public Task DeleteEmployment(string applicantId, string employmentId)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> AddBank(string applicantId, IBankInformation bankDetail)
        {
            throw new NotImplementedException();
        }

        public Task UpdateBank(string applicantId, string bankId, IBankInformation bankDetail)
        {
            throw new NotImplementedException();
        }

        public Task DeleteBank(string applicantId, string bankId)
        {
            throw new NotImplementedException();
        }

        public Task<IApplicant> UpdateApplicant(string applicantId, IApplicant updateApplicantRequest)
        {
            throw new NotImplementedException();
        }

        public Task UpdateEducation(string applicantId, IEducationInformation educationDetail)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
