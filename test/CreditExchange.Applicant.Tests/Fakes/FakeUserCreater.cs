﻿using LendFoundry.Security.Identity;

namespace CreditExchange.Applicant.Fakes
{
    public static class FakeUserCreater 
    {
        public static IUserInfo CreateUser(User userInfo)
        {
            if (userInfo.Username == "TestUserExists")
                throw new UsernameAlreadyExists("TestUserExists");
            return new UserInfo()
            {
                Email = "Test@gmail.com",
                Name = "Test",
                Username = "TestUser"
            };

        }

    }
}
